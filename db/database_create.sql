create table services
(
    service_id int auto_increment
        primary key,
    name       varchar(50)          not null,
    price      double               not null,
    is_deleted tinyint(1) default 0 not null,
    constraint services_name_uindex
        unique (name)
);

create table user_roles
(
    role_id   int auto_increment
        primary key,
    role_name varchar(15) null
);

create table users
(
    user_id      int auto_increment
        primary key,
    first_name   varchar(30) not null,
    last_name    varchar(30) not null,
    username     varchar(20) not null,
    password     varchar(20) not null,
    email        varchar(30) not null,
    phone_number varchar(10) not null,
    role_id      int         null,
    constraint users_email_uindex
        unique (email),
    constraint users_phone_number_uindex
        unique (phone_number),
    constraint users_username_uindex
        unique (username),
    constraint users_user_roles_role_id_fk
        foreign key (role_id) references user_roles (role_id)
);

create table password_tokens
(
    token_id        int auto_increment
        primary key,
    token_content   varchar(8000) not null,
    expiration_date datetime      not null,
    user_id         int           null,
    constraint password_tokens_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create table vehicles_brands
(
    brand_id   int auto_increment
        primary key,
    brand_name varchar(20) not null,
    constraint vehicles_brands_brand_name_uindex
        unique (brand_name)
);

create table vehicles_models
(
    model_id   int auto_increment
        primary key,
    brand_id   int         null,
    model_name varchar(20) not null,
    constraint vehicles_models_vehicles_brands_brand_id_fk
        foreign key (brand_id) references vehicles_brands (brand_id)
);

create table vehicles
(
    vehicle_id int auto_increment
        primary key,
    model_id   int    null,
    year       int(4) not null,
    constraint vehicles_vehicles_models_model_id_fk
        foreign key (model_id) references vehicles_models (model_id)
);

create table registered_vehicles
(
    registered_vehicle_id int auto_increment
        primary key,
    license_plate         varchar(8)  not null,
    vin                   varchar(17) not null,
    vehicle_id            int         null,
    owner_id              int         null,
    constraint registered_vehicles_license_plate_uindex
        unique (license_plate),
    constraint registered_vehicles_vin_uindex
        unique (vin),
    constraint registered_vehicles_users_user_id_fk
        foreign key (owner_id) references users (user_id),
    constraint registered_vehicles_vehicles_vehicle_id_fk
        foreign key (vehicle_id) references vehicles (vehicle_id)
);

create table visit_statuses
(
    status_id int auto_increment
        primary key,
    name      varchar(20) not null,
    constraint service_statuses_name_uindex
        unique (name)
);

create table visits
(
    visit_id              int auto_increment
        primary key,
    start_date            date   not null,
    end_date              date   not null,
    total_price           double not null,
    registered_vehicle_id int    not null,
    visit_status          int    not null,
    constraint visits_registered_vehicles_registered_vehicle_id_fk
        foreign key (registered_vehicle_id) references registered_vehicles (registered_vehicle_id),
    constraint visits_service_statuses_status_id_fk
        foreign key (visit_status) references visit_statuses (status_id)
);

create table services_history
(
    done_service_id int auto_increment
        primary key,
    visit_id        int not null,
    service_id      int not null,
    constraint services_by_visit_services_service_id_fk
        foreign key (service_id) references services (service_id),
    constraint services_by_visit_visits_visit_id_fk
        foreign key (visit_id) references visits (visit_id)
);

