package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.models.UserRole;
import com.telerikacademy.smartgarage.repositories.contracts.UserRolesRepository;
import com.telerikacademy.smartgarage.services.contracts.UserRolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRolesServiceImpl implements UserRolesService {

    private final UserRolesRepository userRolesRepository;

    @Autowired
    public UserRolesServiceImpl(UserRolesRepository userRolesRepository) {
        this.userRolesRepository = userRolesRepository;
    }

    public UserRole getRoleById(int id) {
        return userRolesRepository.getByField("userRoleId", id);
    }
}
