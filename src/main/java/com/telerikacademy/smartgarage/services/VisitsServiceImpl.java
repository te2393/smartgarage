package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.Visits;
import com.telerikacademy.smartgarage.repositories.contracts.VisitsRepository;
import com.telerikacademy.smartgarage.services.contracts.RegisteredVehiclesService;
import com.telerikacademy.smartgarage.services.contracts.VisitStatusesService;
import com.telerikacademy.smartgarage.services.contracts.VisitsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.telerikacademy.smartgarage.utils.helpers.GrantsHelper.isAdmin;

@Service
public class VisitsServiceImpl implements VisitsService {
    private final static int IN_PROGRESS_ID = 1;
    private final static int DONE_ID = 3;

    private final VisitsRepository visitsRepository;
    private final VisitStatusesService statusesService;
    private final RegisteredVehiclesService registeredVehiclesService;

    @Autowired
    public VisitsServiceImpl(VisitsRepository visitsRepository, VisitStatusesService statusesService, RegisteredVehiclesService registeredVehiclesService) {
        this.visitsRepository = visitsRepository;
        this.statusesService = statusesService;
        this.registeredVehiclesService = registeredVehiclesService;
    }

    @Override
    public List<Visits> getAllVisits(Optional<String> keyword, Optional<String> type, Optional<String> startDate,
                                     Optional<String> endDate, Optional<String> sort, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can see all visits");
        }

        return visitsRepository.filter(keyword, type, startDate, endDate, sort);
    }

    @Override
    public Visits getVisitById(int visitId, Users operatingUser) {
//        if (!isAdmin(operatingUser)) {
//            throw new UnauthorizedOperationException("Only employees can see all visits");
//        }

        return visitsRepository.getByField("visitId", visitId);
    }

    @Override
    public void createVisit(Visits visit, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create visits");
        }

        visitsRepository.create(visit);
    }

    @Override
    public void updateVisit(Visits visit, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can update visits");
        }

        visitsRepository.update(visit);
    }

    @Override
    public void deleteVisit(int visitId, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete visits");
        }

        visitsRepository.delete(visitId);
    }

    @Override
    public List<Visits> getVisitsByStatus(String status) {
        return visitsRepository.getByStatus(status);
    }

    @Override
    public List<Visits> getByUser(int userId, Optional<String> keyword, Optional<String> startDate,
                                  Optional<String> endDate, Optional<String> sort) {
        return visitsRepository.getByUser(userId, keyword, startDate, endDate, sort);
    }

    @Override
    public void revertStatus(Visits visitToUpdate, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can revert status of visits");
        }

        if (!visitToUpdate.getStatus().getStatusName().equals("Ready for pick up")) {
            if (visitToUpdate.getStatus().getStatusName().equals("Not started")) {
                visitToUpdate.setStatus(statusesService.getStatusById(IN_PROGRESS_ID));
            } else if (visitToUpdate.getStatus().getStatusName().equals("In progress")) {
                visitToUpdate.setStatus(statusesService.getStatusById(DONE_ID));
            }
        }

        visitsRepository.update(visitToUpdate);
    }

    private void setDefaultVehicleToVisit(int visitId, int vehicleId) {
        Visits visit = visitsRepository.getByField("visitId", visitId);
        visit.setRegisteredVehicle(registeredVehiclesService.getRegisteredVehicleById(vehicleId));
        visitsRepository.update(visit);
    }
}
