package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.contracts.ServicesRepository;
import com.telerikacademy.smartgarage.services.contracts.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.telerikacademy.smartgarage.utils.helpers.GrantsHelper.isAdmin;

@Service
public class ServicesServiceImpl implements ServicesService {

    private final ServicesRepository servicesRepository;

    @Autowired
    public ServicesServiceImpl(ServicesRepository servicesRepository) {
        this.servicesRepository = servicesRepository;
    }

    @Override
    public List<Services> getAllServices() {
        return servicesRepository.getAllServices();
    }

    @Override
    public Services getServiceById(int serviceId) {
        return servicesRepository.getByField("serviceId", serviceId);
    }

    @Override
    public void createService(Services service, Users operatingUser) {

        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create services");
        }

        boolean duplicateServiceName = true;

        try {
            servicesRepository.getByField("serviceName", service.getServiceName());
        } catch (EntityNotFoundException e) {
            duplicateServiceName = false;
        }

        if (duplicateServiceName) {
            throw new DuplicateEntityException("Service", "service name", service.getServiceName());
        }

        servicesRepository.create(service);
    }

    @Override
    public void updateService(Services service, Users operatingUser) {
        boolean duplicateServiceName = true;

        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create services");
        }

        try {
            servicesRepository.getByField("serviceName", service.getServiceName());
        } catch (EntityNotFoundException e) {
            duplicateServiceName = false;
        }

        if (duplicateServiceName) {
            throw new DuplicateEntityException("Service", "service name", service.getServiceName());
        }

        servicesRepository.update(service);
    }

    @Override
    public void deleteService(int serviceId, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create services");
        }

        servicesRepository.deleteService(serviceId);
    }

    @Override
    public List<Services> filter(Optional<String> serviceName, Optional<Double> minPrice, Optional<Double> maxPrice,
                                 Optional<String> sort) {
        return servicesRepository.filter(serviceName, minPrice, maxPrice, sort);
    }

    @Override
    public List<Services> search(Optional<String> keyword) {
        return servicesRepository.search(keyword);
    }
}
