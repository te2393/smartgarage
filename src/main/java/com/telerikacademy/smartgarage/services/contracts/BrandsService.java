package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Brands;
import com.telerikacademy.smartgarage.models.Users;

import java.util.List;

public interface BrandsService {
    Brands getBrandById(int id);

    List<Brands> getAllBrands();

    void createBrand(Brands brand, Users operatingUser);

    void updateBrand(Brands brand, Users operatingUser);

    void deleteBrand(int brandId, Users operatingUser);
}
