package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.UserRole;

public interface UserRolesService {

    UserRole getRoleById(int id);
}
