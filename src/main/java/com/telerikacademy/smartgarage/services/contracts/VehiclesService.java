package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.Vehicles;

import java.util.List;

public interface VehiclesService {
    Vehicles getVehicleById(int id);

    List<Vehicles> getAllVehicles();

    Vehicles getVehicleByModelAndYear(int modelId, int year);

    void createVehicle(Vehicles vehicle, Users operatingUser);

    void updateVehicle(Vehicles vehicle, Users operatingUser);

    void deleteVehicle(int vehicleId, Users operatingUser);

}
