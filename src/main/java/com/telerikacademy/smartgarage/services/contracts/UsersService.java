package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Users;

import java.util.List;
import java.util.Optional;

public interface UsersService {
    List<Users> getAllUsers();

    void createUser(Users user, Users operatingUser);

    Users getByEmail(String email);

    Users getUserById(int id);

    Users getUserByUsername(String username);

    void updateUser(Users userToUpdate, Users operatingUser);

    void deleteUser(int userId, Users operatingUser);

    List<Users> search(Optional<String> keyword, Optional<String> type, Users operatingUser);

    void createPasswordToken(Users operatingUser);

    Users getUserByToken(String token);

    void changePassword(Users operatingUser, String password, String token);

    List<Users> getAllCustomers();

    void changePasswordWithoutToken(Users operatingUser, String password);
}
