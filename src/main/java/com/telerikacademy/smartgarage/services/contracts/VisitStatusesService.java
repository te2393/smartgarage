package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.VisitStatuses;

public interface VisitStatusesService {
    VisitStatuses getStatusById(int id);

}
