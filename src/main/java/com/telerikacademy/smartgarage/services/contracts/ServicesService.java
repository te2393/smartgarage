package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.models.Users;

import java.util.List;
import java.util.Optional;

public interface ServicesService {
    List<Services> getAllServices();

    Services getServiceById(int serviceId);

    void createService(Services service, Users operatingUser);

    void updateService(Services service, Users operatingUser);

    void deleteService(int serviceId, Users operatingUser);

    List<Services> filter(Optional<String> serviceName, Optional<Double> minPrice, Optional<Double> maxPrice, Optional<String> sort);

    List<Services> search(Optional<String> keyword);
}
