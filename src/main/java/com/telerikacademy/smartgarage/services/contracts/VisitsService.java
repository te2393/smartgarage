package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.Visits;

import java.util.List;
import java.util.Optional;

public interface VisitsService {
    List<Visits> getAllVisits(Optional<String> keyword, Optional<String> type, Optional<String> startDate,
                              Optional<String> endDate, Optional<String> sort, Users operatingUser);

    Visits getVisitById(int visitId, Users operatingUser);

    void createVisit(Visits visit, Users operatingUser);

    void updateVisit(Visits visit, Users operatingUser);

    void deleteVisit(int visitId, Users operatingUser);

    List<Visits> getVisitsByStatus(String status);

    List<Visits> getByUser(int userId, Optional<String> keyword, Optional<String> startDate,
                           Optional<String> endDate, Optional<String> sort);

    void revertStatus(Visits visitToUpdate, Users operatingUser);

}
