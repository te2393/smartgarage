package com.telerikacademy.smartgarage.services.contracts;

import com.telerikacademy.smartgarage.models.RegisteredVehicles;
import com.telerikacademy.smartgarage.models.Users;

import java.util.List;
import java.util.Optional;

public interface RegisteredVehiclesService {
    RegisteredVehicles getRegisteredVehicleById(int id);

    RegisteredVehicles getRegisteredVehicleByLicensePlate(String licensePlate);

    List<RegisteredVehicles> getVehiclesByOwner(int userId);

    List<RegisteredVehicles> getAllVehicles();

    void createRegisteredVehicle(RegisteredVehicles vehicle, Users operatingUser);

    void updateRegisteredVehicle(RegisteredVehicles vehicle, Users operatingUser);

    void deleteVehicle(int registeredVehicleId, Users operatingUser);

    List<RegisteredVehicles> filter(Optional<Integer> year,
                                    Optional<Integer> brandId,
                                    Optional<Integer> modelId,
                                    Optional<String> sort,
                                    Users operatingUser);

    List<RegisteredVehicles> search(Optional<String> value, Optional<String> type, Users operatingUser);
}
