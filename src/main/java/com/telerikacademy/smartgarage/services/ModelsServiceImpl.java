package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Models;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.contracts.ModelsRepository;
import com.telerikacademy.smartgarage.services.contracts.ModelsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelsServiceImpl implements ModelsService {

    private final ModelsRepository modelsRepository;

    @Autowired
    public ModelsServiceImpl(ModelsRepository modelsRepository) {
        this.modelsRepository = modelsRepository;
    }

    @Override
    public Models getModelById(int id) {
        return modelsRepository.getByField("modelId", id);
    }

    @Override
    public List<Models> getAllModels() {
        return modelsRepository.getAll();
    }

    @Override
    public void createModel(Models model, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create new brands");
        }

        modelsRepository.create(model);
    }

    @Override
    public void updateModel(Models model, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can update brands");
        }

        modelsRepository.update(model);
    }

    @Override
    public void deleteModel(int modelId, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete brands");
        }

        modelsRepository.delete(modelId);
    }

    @Override
    public List<Models> getModelsByBrandId(int brandId) {
        return modelsRepository.getModelsByBrandId(brandId);
    }

    private boolean isAdmin(Users operatingUser) {
        return operatingUser.getUserRole().getRoleName().equals("Employee");
    }
}
