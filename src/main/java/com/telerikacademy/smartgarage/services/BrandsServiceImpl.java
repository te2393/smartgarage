package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Brands;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.contracts.BrandsRepository;
import com.telerikacademy.smartgarage.services.contracts.BrandsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandsServiceImpl implements BrandsService {

    private final BrandsRepository brandsRepository;

    @Autowired
    public BrandsServiceImpl(BrandsRepository brandsRepository) {
        this.brandsRepository = brandsRepository;
    }

    @Override
    public Brands getBrandById(int id) {
        return brandsRepository.getByField("brandId", id);
    }

    @Override
    public List<Brands> getAllBrands() {
        return brandsRepository.getAll();
    }

    @Override
    public void createBrand(Brands brand, Users operatingUser) {
        boolean brandNameExists = true;

        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create new brands");
        }

        try {
            brandsRepository.getByField("brandName", brand.getBrandName());
        } catch (EntityNotFoundException e) {
            brandNameExists = false;
        }

        if (brandNameExists) {
            throw new DuplicateEntityException("brand", "name", brand.getBrandName());
        }

        brandsRepository.create(brand);
    }

    @Override
    public void updateBrand(Brands brand, Users operatingUser) {

        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can update brands");
        }

        boolean brandNameExists = true;

        try {
            brandsRepository.getByField("brandName", brand.getBrandName());
        } catch (EntityNotFoundException e) {
            brandNameExists = false;
        }

        if (brandNameExists) {
            throw new DuplicateEntityException("brand", "name", brand.getBrandName());
        }

        brandsRepository.update(brand);
    }

    @Override
    public void deleteBrand(int brandId, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete brands");
        }

        brandsRepository.delete(brandId);
    }

    private boolean isAdmin(Users operatingUser) {
        return operatingUser.getUserRole().getRoleName().equals("Employee");
    }
}
