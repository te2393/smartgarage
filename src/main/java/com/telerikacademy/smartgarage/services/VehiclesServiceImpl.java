package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.Vehicles;
import com.telerikacademy.smartgarage.repositories.contracts.VehiclesRepository;
import com.telerikacademy.smartgarage.services.contracts.VehiclesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehiclesServiceImpl implements VehiclesService {
    private final VehiclesRepository vehiclesRepository;

    @Autowired
    public VehiclesServiceImpl(VehiclesRepository vehiclesRepository) {
        this.vehiclesRepository = vehiclesRepository;
    }

    @Override
    public Vehicles getVehicleById(int id) {
        return vehiclesRepository.getByField("vehicleId", id);
    }

    @Override
    public Vehicles getVehicleByModelAndYear(int modelId, int year) {
        return vehiclesRepository.getVehicleByModelAndYear(modelId, year);
    }

    @Override
    public List<Vehicles> getAllVehicles() {
        return vehiclesRepository.getAll();
    }

    @Override
    public void createVehicle(Vehicles vehicle, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can create new vehicles");
        }

        vehiclesRepository.create(vehicle);
    }

    @Override
    public void updateVehicle(Vehicles vehicle, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can update vehicles");
        }

        vehiclesRepository.update(vehicle);
    }

    @Override
    public void deleteVehicle(int vehicleId, Users operatingUser) {
        if (!isAdmin(operatingUser)) {
            throw new UnauthorizedOperationException("Only employees can delete vehicles");
        }

        vehiclesRepository.delete(vehicleId);
    }

    private boolean isAdmin(Users operatingUser) {
        return operatingUser.getUserRole().getRoleName().equals("Employee");
    }
}
