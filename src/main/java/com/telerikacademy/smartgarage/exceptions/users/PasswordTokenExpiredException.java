package com.telerikacademy.smartgarage.exceptions.users;

public class PasswordTokenExpiredException extends RuntimeException {
    public PasswordTokenExpiredException() {
        super("Your reset password token has been expired");
    }
}
