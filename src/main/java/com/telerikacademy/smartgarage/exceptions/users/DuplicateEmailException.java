package com.telerikacademy.smartgarage.exceptions.users;

public class DuplicateEmailException extends RuntimeException {
    public DuplicateEmailException(String value) {
        super(String.format("User with email %s already exists.", value));
    }
}
