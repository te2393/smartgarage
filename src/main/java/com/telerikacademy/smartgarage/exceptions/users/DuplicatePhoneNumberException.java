package com.telerikacademy.smartgarage.exceptions.users;

public class DuplicatePhoneNumberException extends RuntimeException {
    public DuplicatePhoneNumberException(String value) {
        super(String.format("User with phone number %s already exists.", value));
    }
}
