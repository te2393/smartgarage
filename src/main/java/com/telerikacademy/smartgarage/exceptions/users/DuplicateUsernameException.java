package com.telerikacademy.smartgarage.exceptions.users;

public class DuplicateUsernameException extends RuntimeException {
    public DuplicateUsernameException(String value) {
        super(String.format("User with username %s already exists.", value));
    }
}
