package com.telerikacademy.smartgarage.exceptions.services;

public class DuplicateServiceNameException extends RuntimeException {
    public DuplicateServiceNameException(String value) {
        super(String.format("Service with name %s already exists.", value));
    }
}
