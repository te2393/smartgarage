package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicateEmailException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicatePhoneNumberException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicateUsernameException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.dto.CreateUserDTO;
import com.telerikacademy.smartgarage.models.dto.UpdateUserDTO;
import com.telerikacademy.smartgarage.services.contracts.UsersService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UsersController {
    private final UsersService usersService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UsersController(UsersService usersService, UserMapper userMapper, AuthenticationHelper authenticationHelper) {
        this.usersService = usersService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Users> getAllUsers(@RequestParam(required = false) Optional<String> search,
                                   @RequestParam(required = false) Optional<String> value,
                                   @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

//            return usersService.search(search, value, operatingUser);
            return usersService.getAllUsers();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Users getUserById(@PathVariable int id) {
        try {
            return usersService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping()
    public void createUser(@Valid @RequestBody CreateUserDTO userDTO, @RequestHeader HttpHeaders headers) {
        try {
            Users user = userMapper.createDTOToObject(userDTO);
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            usersService.createUser(user, operatingUser);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateUsernameException | DuplicateEmailException | DuplicatePhoneNumberException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{userId}")
    public void updateUser(@PathVariable int userId, @Valid @RequestBody UpdateUserDTO userDTO, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);
            Users userToUpdate = userMapper.updateDTOToObject(userDTO, userId);

            usersService.updateUser(userToUpdate, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEmailException | DuplicatePhoneNumberException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable int userId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            usersService.deleteUser(userId, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
