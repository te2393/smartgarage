package com.telerikacademy.smartgarage.controllers.rest;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.dto.ServiceDTO;
import com.telerikacademy.smartgarage.services.contracts.ServicesService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.utils.helpers.CurrencyHelper;
import com.telerikacademy.smartgarage.utils.mappers.ServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/services")
public class ServicesController {
    private final ServicesService servicesService;
    private final ServiceMapper serviceMapper;
    private final AuthenticationHelper authenticationHelper;
    private final CurrencyHelper currencyHelper;

    @Autowired
    public ServicesController(ServicesService servicesService, ServiceMapper serviceMapper,
                              AuthenticationHelper authenticationHelper, CurrencyHelper currencyHelper) {
        this.servicesService = servicesService;
        this.serviceMapper = serviceMapper;
        this.authenticationHelper = authenticationHelper;
        this.currencyHelper = currencyHelper;
    }

    @GetMapping
    public List<Services> getAll(@RequestParam(required = false) Optional<String> serviceName,
                                 @RequestParam(required = false) Optional<Double> minPrice,
                                 @RequestParam(required = false) Optional<Double> maxPrice,
                                 @RequestParam(required = false) Optional<String> sort) {

        return servicesService.filter(serviceName, minPrice, maxPrice, sort);
    }

    @GetMapping("/{serviceId}")
    public Services getServiceById(@PathVariable int serviceId) {
        try {
            return servicesService.getServiceById(serviceId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/create")
    public void createService(@Valid @RequestBody ServiceDTO serviceDTO, @RequestHeader HttpHeaders headers) {
        try {
            Services serviceToCreate = serviceMapper.createDtoToObject(serviceDTO);
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            servicesService.createService(serviceToCreate, operatingUser);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{serviceId}")
    public void updateService(@Valid @RequestBody ServiceDTO serviceDTO, @PathVariable int serviceId,
                              @RequestHeader HttpHeaders headers) {
        try {
            Services serviceToUpdate = serviceMapper.updateDtoToObject(serviceDTO, serviceId);
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            servicesService.updateService(serviceToUpdate, operatingUser);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/delete/{serviceId}")
    public void deleteService(@PathVariable int serviceId, @RequestHeader HttpHeaders headers) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(headers);

            servicesService.deleteService(serviceId, operatingUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/convert/{serviceId}")
    public Services getWithConvertedPrice(@PathVariable int serviceId, @RequestParam String currency) {
        try {
            Services service = servicesService.getServiceById(serviceId);

            return currencyHelper.convertServiceCurrency(service, currency);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
