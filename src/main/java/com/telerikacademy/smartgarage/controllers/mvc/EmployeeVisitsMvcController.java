package com.telerikacademy.smartgarage.controllers.mvc;

import com.itextpdf.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Brands;
import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.Visits;
import com.telerikacademy.smartgarage.models.dto.CreateVisitDTO;
import com.telerikacademy.smartgarage.services.contracts.*;
import com.telerikacademy.smartgarage.utils.PDFExporter;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.utils.mappers.VehicleMapper;
import com.telerikacademy.smartgarage.utils.mappers.VisitsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/employee/visits")
public class EmployeeVisitsMvcController {
    private final VisitsService visitsService;
    private final ServicesService servicesService;
    private final BrandsService brandsService;
    private final ModelsService modelsService;
    private final VisitsMapper visitsMapper;
    private final UsersService usersService;
    private final VehicleMapper vehicleMapper;
    private final PDFExporter pdfExporter;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public EmployeeVisitsMvcController(VisitsService visitsService, ServicesService servicesService, BrandsService brandsService, ModelsService modelsService, VisitsMapper visitsMapper, UsersService usersService, VehicleMapper vehicleMapper, PDFExporter pdfExporter, AuthenticationHelper authenticationHelper) {
        this.visitsService = visitsService;
        this.servicesService = servicesService;
        this.brandsService = brandsService;
        this.modelsService = modelsService;
        this.visitsMapper = visitsMapper;
        this.usersService = usersService;
        this.vehicleMapper = vehicleMapper;
        this.pdfExporter = pdfExporter;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("brands")
    public List<Brands> populateBrands() {
        return brandsService.getAllBrands();
    }

    @ModelAttribute("services")
    public List<Services> populateServices() {
        return servicesService.getAllServices();
    }

//    @ModelAttribute("models")
//    public List<Models> populateModels(){
//        return modelsService.getAllModels();
//    }

    @GetMapping
    public String showAllVisitsPage(@RequestParam(required = false) Optional<String> keyword,
                                    @RequestParam(required = false) Optional<String> type,
                                    @RequestParam(required = false) Optional<String> startDate,
                                    @RequestParam(required = false) Optional<String> endDate,
                                    @RequestParam(required = false) Optional<String> sort,
                                    Model model, HttpSession session) {
        Users operatingUser;
        try {
            operatingUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("visits", visitsService.getAllVisits(keyword, type, startDate, endDate,
                    sort, operatingUser));
            authenticationHelper.tryGetUser(session);
            return "employee-visits";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }


    @GetMapping("/create")
    public String showCreateVisitPage(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("visitDTO", new CreateVisitDTO());
            model.addAttribute("localDate", LocalDateTime.now());
            return "new-visit";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/detailed/{visitId}")
    public String showDetailedReport(@PathVariable int visitId, Model model, HttpSession session) {
        Users operatingUser;
        try {
            operatingUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("visit", visitsService.getVisitById(visitId, operatingUser));
            model.addAttribute("localDate", LocalDateTime.now());
            return "invoice-details";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @PostMapping("/create")
    public String createVisit(@ModelAttribute("visitDTO") CreateVisitDTO visitDTO, HttpSession session) {
        Users operatingUser;
        try {
            operatingUser = authenticationHelper.tryGetUser(session);
            Visits visit = visitsMapper.createDtoToObject(visitDTO, operatingUser);
            visitsService.createVisit(visit, operatingUser);
            return "coming-soon";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }

    }


    @GetMapping("/customers/search")
    public String showPopulatedCreateVisitPage(@RequestParam Optional<String> keyword,
                                               @RequestParam Optional<String> type,
                                               Model model, HttpSession session) {
        Users operatingUser;
        try {
            operatingUser = authenticationHelper.tryGetUser(session);

            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("visitDTO", new CreateVisitDTO());
            Users user = usersService.search(keyword, type, operatingUser).get(0);
            model.addAttribute("user", user);
            return "new-visit";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            model.addAttribute("errorText", e.getMessage());
            return "new-visit";
        }
    }

    @GetMapping("/{visitId}/export/pdf")
    public String exportToPDF(@PathVariable int visitId,
                              @RequestParam(required = true) String currencySelect,
                              HttpSession session, HttpServletResponse response) throws DocumentException, IOException {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(session);

            Visits visit = visitsService.getVisitById(visitId, operatingUser);

            pdfExporter.export(visit, currencySelect, response);

            return "redirect:/employee/visits/detailed" + visitId;
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        } catch (IOException | BadElementException | URISyntaxException e) {
            return e.getMessage();
        }
    }

    @PostMapping("/detailed/{visitId}/revert")
    public String revertStatus(@PathVariable int visitId, Model model, HttpSession session) {
        try {
            Users operatingUser = authenticationHelper.tryGetUser(session);
            Visits visitToUpdate = visitsService.getVisitById(visitId, operatingUser);
            visitsService.revertStatus(visitToUpdate, operatingUser);

            return "redirect:/employee/visits/detailed/" + visitId;
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }
}
