package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.dto.EmailDTO;
import com.telerikacademy.smartgarage.models.dto.LoginDTO;
import com.telerikacademy.smartgarage.models.dto.PasswordDTO;
import com.telerikacademy.smartgarage.services.contracts.UsersService;
import com.telerikacademy.smartgarage.services.contracts.VisitsService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.utils.helpers.ValidatePasswordTokenHelper;
import com.telerikacademy.smartgarage.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class AuthenticationMvcController {
    private final UsersService userService;
    private final UserMapper userMapper;
    private final VisitsService visitsService;
    private final AuthenticationHelper authenticationHelper;
    private final ValidatePasswordTokenHelper passwordTokenHelper;

    @Autowired
    public AuthenticationMvcController(UsersService userService, UserMapper userMapper, VisitsService visitsService, AuthenticationHelper authenticationHelper, ValidatePasswordTokenHelper passwordTokenHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.visitsService = visitsService;
        this.authenticationHelper = authenticationHelper;
        this.passwordTokenHelper = passwordTokenHelper;
    }


    @GetMapping
    public String redirectFromIndex() {
        return "redirect:/login";
    }

    @GetMapping("/login")
    public String showLoginPage(Model model, HttpSession session) {
        model.addAttribute("login", new LoginDTO());
        return "index";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDTO login,
                              BindingResult bindingResult,
                              HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "index";
        }

        try {
            Users currentUser = authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", currentUser);

            if (currentUser.isEmployee()) {
                return "redirect:/employee/home";
            } else {
                return "redirect:/user/" + currentUser.getUserId();
            }
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "index";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }


    @GetMapping("/forget-password")
    public String showResetPasswordPage(Model model) {
        model.addAttribute("email", new EmailDTO());
        return "forget-pass";
    }

    @PostMapping("/forget-password")
    public String forgetPassword(@Valid @ModelAttribute("email") EmailDTO email, BindingResult bindingResult) {
        try {
            Users operatingUser = userService.getByEmail(email.getEmail());
            userService.createPasswordToken(operatingUser);
            bindingResult.rejectValue("email", "auth_error",
                    "Email has been sent to " + email.getEmail());
            return "forget-pass";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("email", "auth_error", e.getMessage());
            return "forget-pass";
        }
    }

    @GetMapping("/reset-password")
    public String showResetPasswordPage(Model model, @RequestParam("token") String token) {
        passwordTokenHelper.validatePasswordResetToken(token);
        model.addAttribute("recentToken", token);
        model.addAttribute("password", new PasswordDTO());
        return "reset-pass";
    }

    @PostMapping("/reset-password")
    public String savePassword(@Valid @ModelAttribute("password") PasswordDTO password, @RequestParam String token,
                               BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "reset-pass";
        }

        if (!password.getPassword().equals(password.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "password_error", "Password confirmation should match password.");
            return "reset-pass";
        }

        try {
            Users operatingUser = userService.getUserByToken(token);
            userService.changePassword(operatingUser, password.getPassword(), token);
            return "redirect:/";
        } catch (EntityNotFoundException e) {
            bindingResult.rejectValue("confirmPassword", "password_error", e.getMessage());
            return "reset-pass";
        }
    }

}
