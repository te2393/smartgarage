package com.telerikacademy.smartgarage.controllers.mvc;

import com.itextpdf.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.Visits;
import com.telerikacademy.smartgarage.models.dto.ChangePasswordDTO;
import com.telerikacademy.smartgarage.services.contracts.UsersService;
import com.telerikacademy.smartgarage.services.contracts.VisitsService;
import com.telerikacademy.smartgarage.utils.PDFExporter;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/user")
public class UserHomeController {

    private final VisitsService visitsService;
    private final UsersService usersService;
    private final AuthenticationHelper authenticationHelper;
    private final PDFExporter pdfExporter;

    @Autowired
    public UserHomeController(VisitsService visitsService, UsersService usersService, AuthenticationHelper authenticationHelper, PDFExporter pdfExporter) {
        this.visitsService = visitsService;
        this.usersService = usersService;
        this.authenticationHelper = authenticationHelper;
        this.pdfExporter = pdfExporter;
    }

    @GetMapping("/{userId}")
    public String showUserHomePage(@PathVariable int userId, Model model, HttpSession session,
                                   @RequestParam(required = false) Optional<String> keyword,
                                   @RequestParam(required = false) Optional<String> startDate,
                                   @RequestParam(required = false) Optional<String> endDate,
                                   @RequestParam(required = false) Optional<String> sort) {
        try {
            authenticationHelper.tryGetUser(session);
            List<Visits> visits = visitsService.getByUser(userId, keyword, startDate, endDate, sort);
            model.addAttribute("visits", visits);
            model.addAttribute("localDate", LocalDateTime.now());
            return "user-home";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/change-password")
    public String showChangePasswordPage(Model model, HttpSession session) {

        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("password", new ChangePasswordDTO());
            return "change-pass";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @PostMapping("/change-password")
    public String changePassword(@Valid @ModelAttribute("password") ChangePasswordDTO password,
                                 BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "change-pass";
        }

        if (!password.getPassword().equals(password.getConfirmPassword())) {
            bindingResult.rejectValue("confirmPassword", "password_error", "Password confirmation should match password.");
            return "change-pass";
        }

        try {
            Users operatingUser = authenticationHelper.tryGetUser(session);
            usersService.changePasswordWithoutToken(operatingUser, password.getPassword());
            return "redirect:/user" + operatingUser.getUserId();
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/visits/detailed/{visitId}")
    public String showDetailedReport(@PathVariable int visitId, Model model, HttpSession session) {
        Users operatingUser;
        try {
            operatingUser = usersService.getUserById(2);
            model.addAttribute("visit", visitsService.getVisitById(visitId, operatingUser));
            model.addAttribute("localDate", LocalDateTime.now());
            return "invoice-details";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/{visitId}/export/pdf")
    public String exportToPDF(@PathVariable int visitId,
                              @RequestParam(required = true) String currencySelect,
                              HttpSession session, HttpServletResponse response) throws DocumentException, IOException {
        Users operatingUser;
        try {
            operatingUser = usersService.getUserById(2);

            Visits visit = visitsService.getVisitById(visitId, operatingUser);

            pdfExporter.export(visit, currencySelect, response);

            return "redirect:/user/visits/detailed" + visitId;
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        } catch (IOException | BadElementException | URISyntaxException e) {
            return e.getMessage();
        }
    }
}
