package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.services.contracts.UsersService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

@Controller
@RequestMapping("/customer")
public class EmployeeCustomerPageMvcController {
    private final UsersService usersService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public EmployeeCustomerPageMvcController(UsersService usersService, AuthenticationHelper authenticationHelper) {
        this.usersService = usersService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/{userId}")
    public String showCustomerPage(@PathVariable int userId, Model model, HttpSession session) {
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("customer", usersService.getUserById(userId));
            authenticationHelper.tryGetUser(session);
            return "user-profile";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }
}
