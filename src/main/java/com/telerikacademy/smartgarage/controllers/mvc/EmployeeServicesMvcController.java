package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.models.dto.ServiceDTO;
import com.telerikacademy.smartgarage.services.contracts.ServicesService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/services")
public class EmployeeServicesMvcController {
    private final ServicesService servicesService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public EmployeeServicesMvcController(ServicesService servicesService, AuthenticationHelper authenticationHelper) {
        this.servicesService = servicesService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("services")
    public List<Services> getAllServices() {
        return servicesService.getAllServices();
    }

    @GetMapping
    public String showServicesPage(Model model, HttpSession session) {
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("serviceDTO", new ServiceDTO());
            authenticationHelper.tryGetUser(session);
            return "services";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/search")
    public String searchServices(@RequestParam(required = false) Optional<String> keyword, Model model, HttpSession session) {
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("serviceDTO", new ServiceDTO());
            authenticationHelper.tryGetUser(session);
            model.addAttribute("services", servicesService.search(keyword));
            return "services";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/filter")
    public String filterServices(@RequestParam(required = false) Optional<String> name,
                                 @RequestParam(required = false) Optional<Double> demo2,
                                 @RequestParam(required = false) Optional<Double> demo3,
                                 @RequestParam(required = false) Optional<String> sort,
                                 Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("services", servicesService.filter(name, demo2, demo3, sort));
            model.addAttribute("serviceDTO", new ServiceDTO());
            return "services";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @PostMapping("create")
    public String createService(@Valid @RequestBody ServiceDTO serviceDTO) {
        return "services";
    }
}
