package com.telerikacademy.smartgarage.controllers.mvc;

import com.telerikacademy.smartgarage.exceptions.AuthenticationFailureException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicateEmailException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicatePhoneNumberException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicateUsernameException;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.models.dto.CreateUserDTO;
import com.telerikacademy.smartgarage.services.contracts.UsersService;
import com.telerikacademy.smartgarage.utils.helpers.AuthenticationHelper;
import com.telerikacademy.smartgarage.utils.mappers.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/customers")
public class EmployeeCustomersMvcController {
    private final UsersService usersService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public EmployeeCustomersMvcController(UsersService usersService, AuthenticationHelper authenticationHelper, UserMapper userMapper) {
        this.usersService = usersService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @ModelAttribute("customers")
    public List<Users> populateCustomers() {
        return usersService.getAllCustomers();
    }

    @GetMapping
    public String showCustomersPage(Model model, HttpSession session) {
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("customerDTO", new CreateUserDTO());
            authenticationHelper.tryGetUser(session);
            return "customers";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/search")
    public String searchCustomers(@RequestParam(required = false) Optional<String> keyword,
                                  @RequestParam(required = false) Optional<String> type,
                                  Model model, HttpSession session) {
        Users user;
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("customerDTO", new CreateUserDTO());
            user = authenticationHelper.tryGetUser(session);
            List<Users> filteredCustomers = usersService.search(keyword, type, user);
            model.addAttribute("customers", filteredCustomers);
            return "customers";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @GetMapping("/new-customer")
    public String showCreateCustomerPage(Model model, HttpSession session) {
        try {
            model.addAttribute("localDate", LocalDateTime.now());
            model.addAttribute("customerDTO", new CreateUserDTO());
            authenticationHelper.tryGetUser(session);
            return "new-customer";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        }
    }

    @PostMapping("/new-customer")
    public String createCustomer(@Valid @ModelAttribute("customerDTO") CreateUserDTO customerDTO, HttpSession session,
                                 Model model, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "new-customer";
        }

        try {
            Users operatingUser = authenticationHelper.tryGetUser(session);
            Users userToCreate = userMapper.createDTOToObject(customerDTO);
            usersService.createUser(userToCreate, operatingUser);
            return "redirect:/customers";
        } catch (AuthenticationFailureException e) {
            return "redirect:/";
        } catch (DuplicateUsernameException | DuplicateEmailException | DuplicatePhoneNumberException e) {
            bindingResult.rejectValue("email", "duplicateData", e.getMessage());
            return "new-customer";
        }
    }
}
