package com.telerikacademy.smartgarage.utils.mappers;

import com.telerikacademy.smartgarage.models.Brands;
import com.telerikacademy.smartgarage.models.Models;
import com.telerikacademy.smartgarage.models.RegisteredVehicles;
import com.telerikacademy.smartgarage.models.Vehicles;
import com.telerikacademy.smartgarage.models.dto.*;
import com.telerikacademy.smartgarage.services.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleMapper {
    private final BrandsService brandsService;
    private final ModelsService modelsService;
    private final VehiclesService vehiclesService;
    private final UsersService usersService;
    private final RegisteredVehiclesService registeredVehiclesService;


    @Autowired
    public VehicleMapper(BrandsService brandsService,
                         ModelsService modelsService,
                         VehiclesService vehiclesService,
                         UsersService usersService,
                         RegisteredVehiclesService registeredVehiclesService) {

        this.brandsService = brandsService;
        this.modelsService = modelsService;
        this.vehiclesService = vehiclesService;
        this.usersService = usersService;
        this.registeredVehiclesService = registeredVehiclesService;
    }

    public Brands createBrandDTOtoObject(BrandDTO brandDTO) {
        Brands brand = new Brands();

        brand.setBrandName(brandDTO.getBrandName());

        return brand;
    }

    public Brands updateBrandDTOtoObject(BrandDTO brandDTO, int brandId) {
        Brands brand = brandsService.getBrandById(brandId);

        brand.setBrandName(brandDTO.getBrandName());

        return brand;
    }

    public Models createModelDTOtoObject(ModelDTO modelDTO) {
        Models model = new Models();

        model.setModelName(modelDTO.getModelName());
        model.setBrand(brandsService.getBrandById(modelDTO.getBrandId()));

        return model;
    }

    public Models updateModelDTOtoObject(ModelDTO modelDTO, int modelId) {
        Models model = modelsService.getModelById(modelId);

        model.setModelName(modelDTO.getModelName());
        model.setBrand(brandsService.getBrandById(modelDTO.getBrandId()));

        return model;
    }

    public Vehicles createVehicleDTOtoObject(VehicleDTO vehicleDTO) {
        Vehicles vehicle = new Vehicles();

        vehicle.setModel(modelsService.getModelById(vehicleDTO.getModelId()));
        vehicle.setYear(vehicleDTO.getYear());

        return vehicle;
    }

    public Vehicles updateVehicleDTOtoObject(VehicleDTO vehicleDTO, int vehicleId) {
        Vehicles vehicle = vehiclesService.getVehicleById(vehicleId);

        vehicle.setModel(modelsService.getModelById(vehicleDTO.getModelId()));
        vehicle.setYear(vehicleDTO.getYear());

        return vehicle;
    }

    public RegisteredVehicles createRegVehicleDTOtoObject(CreateRegVehicleDTO createRegVehicleDTO) {
        RegisteredVehicles vehicle = new RegisteredVehicles();

        vehicle.setVehicle(vehiclesService.getVehicleById(createRegVehicleDTO.getVehicleId()));
        vehicle.setLicensePlate(createRegVehicleDTO.getLicensePlate());
        vehicle.setVin(createRegVehicleDTO.getVin());
        vehicle.setOwner(usersService.getUserById(createRegVehicleDTO.getOwnerId()));

        return vehicle;
    }

    public RegisteredVehicles createRegVehicleDTOtoObject(CreateVisitDTO registeredVehicleDTO) {
        RegisteredVehicles vehicle = new RegisteredVehicles();
        vehicle.setVin(registeredVehicleDTO.getVin());

        return vehicle;
    }

    public RegisteredVehicles updateRegVehicleDTOtoObject(UpdateRegVehicleDTO updateRegVehicleDTO, int regVehicleId) {
        RegisteredVehicles vehicle = registeredVehiclesService.getRegisteredVehicleById(regVehicleId);

        vehicle.setVehicle(vehiclesService.getVehicleById(updateRegVehicleDTO.getVehicleId()));
        vehicle.setOwner(usersService.getUserById(updateRegVehicleDTO.getOwnerId()));
        vehicle.setLicensePlate(updateRegVehicleDTO.getLicensePlate());

        return vehicle;
    }
}
