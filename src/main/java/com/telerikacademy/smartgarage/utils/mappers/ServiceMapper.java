package com.telerikacademy.smartgarage.utils.mappers;

import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.models.dto.ServiceDTO;
import com.telerikacademy.smartgarage.services.contracts.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceMapper {
    private final ServicesService servicesService;

    @Autowired
    public ServiceMapper(ServicesService servicesService) {
        this.servicesService = servicesService;
    }

    public Services createDtoToObject(ServiceDTO serviceDTO) {
        Services service = new Services();

        service.setServiceName(serviceDTO.getServiceName());
        service.setPrice(serviceDTO.getPrice());

        return service;
    }

    public Services updateDtoToObject(ServiceDTO serviceDTO, int serviceId) {
        Services service = servicesService.getServiceById(serviceId);

        service.setServiceName(serviceDTO.getServiceName());
        service.setPrice(serviceDTO.getPrice());

        return service;
    }
}
