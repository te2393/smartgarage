package com.telerikacademy.smartgarage.utils;

import com.telerikacademy.smartgarage.models.PasswordTokens;
import com.telerikacademy.smartgarage.models.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

@Component
public class EmailHelper {
    private static final String PLATFORM_MAIL = "smartgaragee@gmail.com";
    private static final String NEW_CUSTOMER_LOGIN = "Login credentials for new customer";
    private static final String NEW_CUSTOMER_TEXT = "Hello,%n%n here are your login credentials for our platform: %n" +
            "username: %s%n, password: %s%n%n" +
            "Now you can log in SmartGarage and change your password.%n" +
            "Best regards,%n" +
            "SmartGarage Team";
    private static final String RESET_PASSWORD_TEXT = "Ops, it looks like you forgot your password, here is link for setting new one,%n" +
            "%s%n%n" +
            "Best regards,%n" +
            "SmartGarage Team";


    private final JavaMailSender emailSender;

    @Autowired
    public EmailHelper(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public void constructLoginCredentialsEmail(Users user) {
        String message = String.format(NEW_CUSTOMER_TEXT, user.getUsername(), user.getPassword());
        constructEmail("New customer login credentials", message, user);
    }

    public void constructResetTokenEmail(Users user, PasswordTokens token) {
        String url = "http://localhost:8080/reset-password?token=" + token.getTokenContent();
        String message = String.format(RESET_PASSWORD_TEXT, url);
        constructEmail("Reset password", message, user);
    }

    private void constructEmail(String subject, String message, Users user) {
        SimpleMailMessage email = new SimpleMailMessage();
        email.setSubject(subject);
        email.setText(message);
        email.setTo(user.getEmail());
        email.setFrom(PLATFORM_MAIL);
        emailSender.send(email);
    }
}
