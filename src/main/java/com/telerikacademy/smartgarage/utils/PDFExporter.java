package com.telerikacademy.smartgarage.utils;

import com.itextpdf.text.BadElementException;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.telerikacademy.smartgarage.models.Services;
import com.telerikacademy.smartgarage.models.Visits;
import com.telerikacademy.smartgarage.utils.helpers.CurrencyHelper;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.stream.Stream;

@Component
public class PDFExporter {

    private static final DecimalFormat df = new DecimalFormat("0.00");

    private double totalPrice = 0;
    private String currency = "BGN";

    private final CurrencyHelper currencyHelper;

    public PDFExporter(CurrencyHelper currencyHelper) {
        this.currencyHelper = currencyHelper;
    }

    public void export(Visits visit, String currency, HttpServletResponse response) throws URISyntaxException, BadElementException, IOException, DocumentException {
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, response.getOutputStream());

        document.open();

        Paragraph p = new Paragraph(new Phrase("Invoice", FontFactory.getFont(FontFactory.COURIER_BOLD, 20)));
        p.setAlignment(Element.ALIGN_CENTER);
        p.setSpacingAfter(20f);
        document.add(p);

        PdfPTable invoiceDetails = new PdfPTable(2);
        addInvoiceDetailsHeader(invoiceDetails);
        addInvoiceDetailsRows(visit, invoiceDetails);
        invoiceDetails.setSpacingBefore(3f);
        invoiceDetails.setSpacingAfter(20f);

        PdfPTable servicesList = new PdfPTable(3);
        addServicesTableHeader(servicesList);
        addServicesTableRows(visit, currency, servicesList);
        servicesList.setSpacingBefore(3f);
        servicesList.setSpacingAfter(3f);

        Paragraph p1 = new Paragraph(new Phrase("Invoice details", FontFactory.getFont(FontFactory.COURIER_BOLD, 15)));
        p1.setAlignment(Element.ALIGN_CENTER);
        p1.setSpacingAfter(10f);
        document.add(p1);
        document.add(invoiceDetails);

        Paragraph p2 = new Paragraph(new Phrase("Services List", FontFactory.getFont(FontFactory.COURIER_BOLD, 15)));
        p2.setAlignment(Element.ALIGN_CENTER);
        p2.setSpacingAfter(10f);
        document.add(p2);

        document.add(servicesList);

        Paragraph totalPriceParagraph = new Paragraph(new Phrase("TotalPrice: " + df.format(totalPrice) + " " + currency, FontFactory.getFont(FontFactory.TIMES_BOLD, 15)));
        totalPriceParagraph.setAlignment(Element.ALIGN_RIGHT);
        totalPriceParagraph.setSpacingAfter(5f);
        document.add(totalPriceParagraph);
        document.close();
    }

    private void addInvoiceDetailsHeader(PdfPTable table) {
        Stream.of("To", "From")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(Color.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addInvoiceDetailsRows(Visits visit, PdfPTable table) {
        table.addCell(visit.getRegisteredVehicle().getOwner().getFirstName() + " " + visit.getRegisteredVehicle().getOwner().getLastName() + "\n\n"
                + visit.getRegisteredVehicle().getOwner().getEmail() + "\n\n"
                + visit.getRegisteredVehicle().getOwner().getPhoneNumber() + "\n\n"
                + visit.getRegisteredVehicle().getLicensePlate());
        table.addCell("Smart Garage Service\n\n"
                + "21 Bulgaria str., Sunny Beach, Burgas\n\n"
                + "smartgaragee@gmail.com\n\n"
                + "0896525154");
    }

    private void addServicesTableHeader(PdfPTable table) {
        Stream.of("Code", "Service name", "Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(Color.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });
    }

    private void addServicesTableRows(Visits visit, String currency, PdfPTable table) {
        double totalPrice = 0;
        for (Services service : visit.getServicesList()) {
            Double servicePrice = currencyHelper.convertServiceCurrency(service, currency).getPrice();
            totalPrice += servicePrice;
            table.addCell("#" + service.getServiceId());
            table.addCell(service.getServiceName());
            table.addCell(df.format(servicePrice) + " " + currency);
        }

        this.totalPrice = totalPrice;
        this.currency = currency;
    }
}
