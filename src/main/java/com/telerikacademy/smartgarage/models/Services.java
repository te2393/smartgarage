package com.telerikacademy.smartgarage.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "services")
public class Services {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "service_id")
    private int serviceId;

    @Column(name = "name")
    private String serviceName;

    @Column(name = "price")
    private double price;

    @Column(name = "is_deleted")
    private boolean isDeleted;

    @JsonIgnore
    @ManyToMany(mappedBy = "servicesList", fetch = FetchType.EAGER)
    private Set<Visits> visits;

    public Services() {
    }

    public Services(int serviceId, String serviceName, double price, boolean isDeleted, Set<Visits> visits) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.price = price;
        this.isDeleted = isDeleted;
        this.visits = visits;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public Set<Visits> getVisits() {
        return visits;
    }

    public void setVisits(Set<Visits> visits) {
        this.visits = visits;
    }
}
