package com.telerikacademy.smartgarage.models.dto;

import java.util.List;

public class CreateVisitDTO {
    private String firstName;

    private String lastName;

    private String email;

    private String phone;

    private int brandId;

    private int modelId;

    private int year;

    private String licensePlate;

    private String vin;

    private List<Integer> serviceIds;

    public CreateVisitDTO() {
    }

    public CreateVisitDTO(String firstName, String lastName, String email, String phone, int brandId,
                          int modelId, int year, String licensePlate, String vin, List<Integer> serviceIds) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.brandId = brandId;
        this.modelId = modelId;
        this.year = year;
        this.licensePlate = licensePlate;
        this.vin = vin;
        this.serviceIds = serviceIds;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public List<Integer> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(List<Integer> serviceIds) {
        this.serviceIds = serviceIds;
    }
}
