package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserDTO {

    @NotNull(message = "Email cannot be empty")
    @Email(message = "Email is not valid", regexp = "^(.+)@(.+)$")
    private String email;

    @NotNull(message = "Phone number cannot be empty")
    @Size(min = 10, max = 10, message = "Phone number should be exactly 10 digits")
    private String phoneNumber;

    public UpdateUserDTO() {
    }

    public UpdateUserDTO(String email, String phoneNumber) {
        this.email = email;
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
