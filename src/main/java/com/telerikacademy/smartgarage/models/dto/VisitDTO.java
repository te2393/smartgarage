package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class VisitDTO {

    @NotNull(message = "Registered vehicle cannot be empty")
    private int registeredVehicleId;

    @NotEmpty(message = "Services list cannot be empty")
    private List<Integer> servicesIds;

    public VisitDTO() {
    }

    public VisitDTO(int registeredVehicleId, List<Integer> servicesIds) {
        this.registeredVehicleId = registeredVehicleId;
        this.servicesIds = servicesIds;
    }

    public int getRegisteredVehicleId() {
        return registeredVehicleId;
    }

    public void setRegisteredVehicleId(int registeredVehicleId) {
        this.registeredVehicleId = registeredVehicleId;
    }

    public List<Integer> getServicesIds() {
        return servicesIds;
    }

    public void setServicesIds(List<Integer> servicesIds) {
        this.servicesIds = servicesIds;
    }
}
