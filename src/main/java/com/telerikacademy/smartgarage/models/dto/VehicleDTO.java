package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class VehicleDTO {

    @NotNull(message = "Model id cannot be empty")
    private int modelId;

    @NotNull(message = "Vehicle year cannot be empty")
    @Size(min = 1886, message = "Vehicle year should be greater than 1886")
    private int year;

    public VehicleDTO() {
    }

    public VehicleDTO(int modelId, int year) {
        this.modelId = modelId;
        this.year = year;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
