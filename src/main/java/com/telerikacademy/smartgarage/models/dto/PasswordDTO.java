package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class PasswordDTO {
    @NotEmpty(message = "Password cannot be empty")
    @Pattern(
            message = "Password should contain at least 8 characters, including uppercase," +
                    " lowercase, digit and special symbol(!@#$+*)",
            regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$+*])(?=\\S+$).{8,}$")
    private String password;

    @NotEmpty(message = "Password confirmation cannot be empty")
    private String confirmPassword;

    public PasswordDTO() {
    }

    public PasswordDTO(String password, String confirmPassword) {
        this.password = password;
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
