package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateRegVehicleDTO {

    @NotNull(message = "License plate cannot be empty")
    @Size(min = 7, max = 8, message = "License plate should be between 7 and 8 symbols")
    private String licensePlate;

    @NotNull(message = "Owner id cannot be empty")
    private int ownerId;

    @NotNull(message = "Vehicle id cannot be empty")
    private int vehicleId;

    public UpdateRegVehicleDTO() {
    }

    public UpdateRegVehicleDTO(String licensePlate, int ownerId, int vehicleId) {
        this.licensePlate = licensePlate;
        this.ownerId = ownerId;
        this.vehicleId = vehicleId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }
}
