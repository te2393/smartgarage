package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ModelDTO {

    @NotNull(message = "Model name cannot be empty")
    @Size(min = 2, max = 32, message = "Model name should be between 2 and 32 symbols")
    private String modelName;

    @NotNull(message = "Model id cannot be empty")
    private int brandId;

    public ModelDTO() {
    }

    public ModelDTO(String modelName, int brandId) {
        this.modelName = modelName;
        this.brandId = brandId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
}
