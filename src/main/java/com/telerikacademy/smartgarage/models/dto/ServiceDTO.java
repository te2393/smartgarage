package com.telerikacademy.smartgarage.models.dto;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class ServiceDTO {

    @Size(min = 2, max = 32, message = "Service name should be between 2 and 32 symbols")
    private String serviceName;

    @PositiveOrZero(message = "Service price should be non-negative number")
    private double price;

    public ServiceDTO() {
    }

    public ServiceDTO(String serviceName, double price) {
        this.serviceName = serviceName;
        this.price = price;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
