package com.telerikacademy.smartgarage.models;

import javax.persistence.*;

@Entity
@Table(name = "vehicles")
public class Vehicles {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vehicle_id")
    private int vehicleId;

    @ManyToOne
    @JoinColumn(name = "model_id")
    private Models model;

    @Column(name = "year")
    private int year;

    public Vehicles() {
    }

    public Vehicles(int vehicleId, Models model, int year) {
        this.vehicleId = vehicleId;
        this.model = model;
        this.year = year;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public Models getModel() {
        return model;
    }

    public void setModel(Models model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
