package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Brands;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.BrandsRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class BrandsRepositoryImpl extends AbstractCRUDRepository<Brands> implements BrandsRepository {

    @Autowired
    public BrandsRepositoryImpl(SessionFactory sessionFactory) {
        super(Brands.class, sessionFactory);
    }
}
