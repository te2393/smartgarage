package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Models;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.ModelsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ModelsRepositoryImpl extends AbstractCRUDRepository<Models> implements ModelsRepository {

    @Autowired
    public ModelsRepositoryImpl(SessionFactory sessionFactory) {
        super(Models.class, sessionFactory);
    }

    @Override
    public List<Models> getModelsByBrandId(int brandId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Models> query = session.createQuery("from Models where brand.brandId = :brandId", Models.class);
            query.setParameter("brandId", brandId);

            return query.list();
        }
    }
}
