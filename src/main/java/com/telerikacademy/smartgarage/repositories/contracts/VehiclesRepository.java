package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Vehicles;

public interface VehiclesRepository extends BaseCRUDRepository<Vehicles> {
    Vehicles getVehicleByModelAndYear(int modelId, int year);
}
