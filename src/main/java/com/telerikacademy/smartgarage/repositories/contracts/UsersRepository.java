package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Users;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends BaseCRUDRepository<Users> {

    List<Users> search(Optional<String> keyword, Optional<String> type);

    List<Users> getAllCustomers();
}
