package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Visits;

import java.util.List;
import java.util.Optional;

public interface VisitsRepository extends BaseCRUDRepository<Visits> {

    List<Visits> filter(Optional<String> keyword, Optional<String> type, Optional<String> startDate,
                        Optional<String> endDate, Optional<String> sort);

    List<Visits> getByStatus(String status);

    List<Visits> getByUser(int userId, Optional<String> keyword, Optional<String> startDate,
                           Optional<String> endDate, Optional<String> sort);

    void makeVisitsWithDefaultVehicle(int userId);
}
