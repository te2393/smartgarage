package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.PasswordTokens;
import com.telerikacademy.smartgarage.models.Users;

public interface PasswordTokensRepository extends BaseCRUDRepository<PasswordTokens> {

    Users getUserByToken(String token);

    PasswordTokens getTokenByContent(String token);
}
