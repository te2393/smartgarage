package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.RegisteredVehicles;

import java.util.List;
import java.util.Optional;

public interface RegisteredVehicleRepository extends BaseCRUDRepository<RegisteredVehicles> {

    List<RegisteredVehicles> filter(Optional<Integer> year,
                                    Optional<Integer> brandId,
                                    Optional<Integer> modelId,
                                    Optional<String> sort);

    List<RegisteredVehicles> search(Optional<String> phoneNumber, Optional<String> type);

    void deleteVehiclesByOwner(int userId);

    List<RegisteredVehicles> getVehiclesByOwner(int userId);
}
