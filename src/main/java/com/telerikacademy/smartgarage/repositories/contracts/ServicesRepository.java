package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Services;

import java.util.List;
import java.util.Optional;

public interface ServicesRepository extends BaseCRUDRepository<Services> {

    List<Services> filter(Optional<String> serviceName, Optional<Double> minPrice, Optional<Double> maxPrice,
                          Optional<String> sort);

    List<Services> search(Optional<String> keyword);

    void deleteService(int serviceId);

    List<Services> getAllServices();

}
