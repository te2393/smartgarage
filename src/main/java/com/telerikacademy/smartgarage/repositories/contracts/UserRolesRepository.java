package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.UserRole;

public interface UserRolesRepository extends BaseCRUDRepository<UserRole> {
}
