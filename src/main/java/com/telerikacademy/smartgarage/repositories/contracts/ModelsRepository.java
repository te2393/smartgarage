package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Models;

import java.util.List;

public interface ModelsRepository extends BaseCRUDRepository<Models> {

    List<Models> getModelsByBrandId(int brandId);
}
