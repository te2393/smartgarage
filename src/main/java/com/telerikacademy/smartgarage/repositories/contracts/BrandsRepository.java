package com.telerikacademy.smartgarage.repositories.contracts;

import com.telerikacademy.smartgarage.models.Brands;

public interface BrandsRepository extends BaseCRUDRepository<Brands> {
}
