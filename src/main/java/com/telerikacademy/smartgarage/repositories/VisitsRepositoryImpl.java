package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.Visits;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.RegisteredVehicleRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitsRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class VisitsRepositoryImpl extends AbstractCRUDRepository<Visits> implements VisitsRepository {
    private static final int DEFAULT_VEHICLE_ID = 28;

    private final RegisteredVehicleRepository vehicleRepository;

    public VisitsRepositoryImpl(SessionFactory sessionFactory, RegisteredVehicleRepository vehicleRepository) {
        super(Visits.class, sessionFactory);
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public List<Visits> filter(Optional<String> keyword, Optional<String> type, Optional<String> startDate,
                               Optional<String> endDate, Optional<String> sort) {
        var queryString = new StringBuilder(" from Visits ");
        var filter = new ArrayList<String>();
        var queryParams = new HashMap<String, Object>();

        try (Session session = sessionFactory.openSession()) {
//            Query<Visits> query = session.createQuery("from Visits where lower(concat(registeredVehicle.owner.firstName,' ',registeredVehicle.owner.lastName)) like lower(:value) order by registeredVehicle.owner.firstName", Visits.class);
            keyword.ifPresent(fieldValue -> {
                if (type.get().equals("name")) {
                    filter.add("lower(concat(registeredVehicle.owner.firstName,' ',registeredVehicle.owner.lastName)) like lower(:value) ");
                    queryParams.put("value", "%" + fieldValue + "%");
                } else if (type.get().equals("licensePlate")) {
                    filter.add(" registeredVehicle.licensePlate like :value ");
                    queryParams.put("value", fieldValue);
                } else if (type.get().equals("model")) {
                    filter.add("lower(concat(registeredVehicle.vehicle.model.brand.brandName,' ',registeredVehicle.vehicle.model.modelName)) like lower(:value) ");
                    queryParams.put("value", fieldValue);
                }
            });

            if (startDate.isPresent() || endDate.isPresent()) {
                if (!generateDateQueryString(startDate, endDate).equals("")) {
                    filter.add(generateDateQueryString(startDate, endDate));
                }
            }

            if (!filter.isEmpty()) {
                queryString.append("where ");
                for (int i = 0; i < filter.size(); i++) {
                    if (i + 1 == filter.size()) {
                        queryString.append(filter.get(i));
                    } else {
                        queryString.append(filter.get(i)).append(" and ");
                    }
                }
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<Visits> queryList = session.createQuery(queryString.toString(), Visits.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    @Override
    public List<Visits> getByStatus(String status) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visits> query = session.createQuery("from Visits where status.statusName = :statusName",
                    Visits.class);
            query.setParameter("statusName", status);
            query.setMaxResults(5);
            return query.list();
        }
    }

    public List<Visits> getByUser(int userId, Optional<String> keyword, Optional<String> startDate,
                                  Optional<String> endDate, Optional<String> sort) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("from Visits where registeredVehicle.owner.id = :userId ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            queryParams.put("userId", userId);

            keyword.ifPresent(fieldValue -> {
                filter.add(" lower(concat(registeredVehicle.vehicle.model.brand.brandName,' ',registeredVehicle.vehicle.model.modelName)) like lower(:value)");
                queryParams.put("value", "%" + fieldValue + "%");
            });

            if (startDate.isPresent() || endDate.isPresent()) {
                String dateQuery = generateDateQueryString(startDate, endDate);
                if (!dateQuery.equals("")) {
                    filter.add(dateQuery);
                }
            }

            if (!filter.isEmpty()) {
                for (String s : filter) {
                    queryString.append(" and ").append(s);
                }
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<Visits> queryList = session.createQuery(queryString.toString(), Visits.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    @Override
    public void makeVisitsWithDefaultVehicle(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Visits> query = session.createQuery("from Visits where registeredVehicle.owner.userId = :userId",
                    Visits.class);
            query.setParameter("userId", userId);

            List<Visits> visitsToUpdate = query.list();
            for (Visits visits : visitsToUpdate) {
                session.beginTransaction();
                visits.setRegisteredVehicle(vehicleRepository.getByField("registeredVehicleId", DEFAULT_VEHICLE_ID));
                session.update(visits);
                session.getTransaction().commit();
            }
        }
    }

    public String generateDateQueryString(Optional<String> startDate, Optional<String> endDate) {
        var queryString = new StringBuilder();

        if (startDate.isEmpty() && endDate.isEmpty()) {
            return queryString.toString();
        }

        if (startDate.isPresent() && endDate.isPresent()) {
            if (!startDate.get().equals("") && !endDate.get().equals("")) {
                queryString.append(" startDate = ")
                        .append("'" + startDate.get() + "'" + " and endDate =")
                        .append("'" + endDate.get() + "'");
                return queryString.toString();
            }
        }

        if (startDate.isPresent() && endDate.get().isEmpty()) {
            if (!startDate.get().equals("")) {
                queryString.append("startDate = ")
                        .append("'" + startDate.get() + "'");
            }
        }

        if (startDate.get().isEmpty() && endDate.isPresent()) {
            if (!endDate.get().equals("")) {
                queryString.append(" endDate = ")
                        .append("'" + endDate.get() + "'");
            }
        }

        return queryString.toString();
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        switch (params[0]) {
            case "price":
                queryString.append(" totalPrice ");
                break;
            case "name":
                queryString.append(" registeredVehicle.owner.firstName ");
                break;
            case "date":
                queryString.append(" startDate ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        return queryString.toString();
    }

}
