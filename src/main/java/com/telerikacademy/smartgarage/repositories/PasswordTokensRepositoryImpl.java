package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.PasswordTokens;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.PasswordTokensRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class PasswordTokensRepositoryImpl extends AbstractCRUDRepository<PasswordTokens> implements PasswordTokensRepository {
    public PasswordTokensRepositoryImpl(SessionFactory sessionFactory) {
        super(PasswordTokens.class, sessionFactory);
    }

    public Users getUserByToken(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<Users> query = session.createQuery
                    ("select u from Users u join u.passwordToken p where p.tokenContent = :passwordToken",
                            Users.class);
            query.setParameter("passwordToken", token);

            Users user = query.list().get(0);
            if (user == null) {
                throw new EntityNotFoundException("Not created token");
            }

            return user;
        }
    }

    public PasswordTokens getTokenByContent(String token) {
        try (Session session = sessionFactory.openSession()) {
            Query<PasswordTokens> query = session.createQuery
                    (" select p from PasswordTokens p where tokenContent = :tokenContent", PasswordTokens.class);
            query.setParameter("tokenContent", token);

            PasswordTokens passwordToken = query.getSingleResult();
            if (passwordToken == null) {
                throw new EntityNotFoundException("Token is missing");
            }

            return passwordToken;
        }
    }
}
