package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.RegisteredVehicles;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.RegisteredVehicleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class RegisteredVehicleRepositoryImpl extends AbstractCRUDRepository<RegisteredVehicles>
        implements RegisteredVehicleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RegisteredVehicleRepositoryImpl(SessionFactory sessionFactory) {
        super(RegisteredVehicles.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public List<RegisteredVehicles> search(Optional<String> keyword, Optional<String> type) {
        try (Session session = sessionFactory.openSession()) {
            StringBuilder queryString = new StringBuilder();
            var queryParams = new HashMap<String, Object>();
            queryString.append(" from RegisteredVehicles ");

            keyword.ifPresent(fieldValue -> {
                if (type.get().equals("name")) {
                    queryString.append("where lower(owner.firstName) like lower(:value) or lower(owner.lastName) like lower(:value)");
                    queryParams.put("value", "%" + fieldValue + "%");
                } else if (type.get().equals("email")) {
                    queryString.append("where owner.email = :value");
                    queryParams.put("value", fieldValue);
                }
            });

            Query<RegisteredVehicles> query = session.createQuery(queryString.toString(), RegisteredVehicles.class);
            query.setProperties(queryParams);

            return query.list();
        }
    }

    public void deleteVehiclesByOwner(int userId) {
        try (Session session = sessionFactory.openSession()) {
            if (!getVehiclesByOwner(userId).isEmpty()) {
                for (RegisteredVehicles vehicle : getVehiclesByOwner(userId)) {
                    session.beginTransaction();
                    session.delete(vehicle);
                    session.getTransaction().commit();
                }
            }
        }
    }

    @Override
    public List<RegisteredVehicles> getVehiclesByOwner(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<RegisteredVehicles> query = session.createQuery(
                    "from RegisteredVehicles where owner.userId = :userId", RegisteredVehicles.class);
            query.setParameter("userId", userId);

            return query.list();
        }
    }


    public List<RegisteredVehicles> filter(Optional<Integer> year,
                                           Optional<Integer> brandId,
                                           Optional<Integer> modelId,
                                           Optional<String> sort) {

        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder(" from RegisteredVehicles ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();

            year.ifPresent(value -> {
                filter.add(" vehicle.year = :year ");
                queryParams.put("year", value);
            });

            brandId.ifPresent(value -> {
                filter.add(" vehicle.model.brand.brandId = :brandId ");
                queryParams.put("brandId", value);
            });

            modelId.ifPresent(value -> {
                filter.add(" vehicle.model.modelId = :modelId");
                queryParams.put("modelId", value);
            });


            if (!filter.isEmpty()) {
                queryString.append("where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<RegisteredVehicles> queryList = session.createQuery(queryString.toString(), RegisteredVehicles.class);
            queryList.setProperties(queryParams);

            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");

        if (value.isEmpty()) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        switch (params[0]) {
            case "year":
                queryString.append(" vehicle.year ");
                break;
            case "brandId":
                queryString.append(" vehicle.model.brand.brandId ");
                break;
            case "modelId":
                queryString.append(" vehicle.model.modelId ");
                break;
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }

        return queryString.toString();
    }
}
