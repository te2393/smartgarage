package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.models.UserRole;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UserRolesRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRolesRepositoryImpl extends AbstractCRUDRepository<UserRole> implements UserRolesRepository {

    @Autowired
    public UserRolesRepositoryImpl(SessionFactory sessionFactory) {
        super(UserRole.class, sessionFactory);
    }
}
