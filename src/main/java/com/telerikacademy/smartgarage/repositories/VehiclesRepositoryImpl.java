package com.telerikacademy.smartgarage.repositories;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.models.Vehicles;
import com.telerikacademy.smartgarage.repositories.abstractrepos.AbstractCRUDRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VehiclesRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class VehiclesRepositoryImpl extends AbstractCRUDRepository<Vehicles> implements VehiclesRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public VehiclesRepositoryImpl(SessionFactory sessionFactory) {
        super(Vehicles.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Vehicles getVehicleByModelAndYear(int modelId, int year) {
        try (Session session = sessionFactory.openSession()) {
            Query<Vehicles> query = session.createQuery("from Vehicles where model.modelId = :modelId " +
                    "and year = :year", Vehicles.class);
            query.setParameter("modelId", modelId);
            query.setParameter("year", year);

            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Vehicle with modelId " + modelId + " and year " + year + "was not found");
            }

            return query.list().get(0);
        }
    }
}
