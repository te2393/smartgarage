package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.repositories.contracts.UserRolesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.smartgarage.Helpers.createMockRole;

@ExtendWith(MockitoExtension.class)
public class UserRolesServiceImplTests {

    @Mock
    UserRolesRepository userRolesRepository;

    @InjectMocks
    UserRolesServiceImpl rolesService;

    @Test
    void getById_should_callRepository() {
        // Arrange
        var role = createMockRole("role");
        Mockito.when(userRolesRepository.getByField("userRoleId", role.getUserRoleId()))
                .thenReturn(role);

        // Act
        var result = rolesService.getRoleById(role.getUserRoleId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(role.getUserRoleId(), result.getUserRoleId()),
                () -> Assertions.assertEquals(role.getRoleName(), result.getRoleName())
        );
    }
}
