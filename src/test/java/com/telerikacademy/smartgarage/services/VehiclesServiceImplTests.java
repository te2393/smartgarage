package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.VehiclesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class VehiclesServiceImplTests {

    @Mock
    VehiclesRepository vehiclesRepository;

    @InjectMocks
    VehiclesServiceImpl vehiclesService;

    @Test
    void getById_should_callRepository() {
        // Arrange
        var vehicle = createMockVehicle("model", "brand");
        Mockito.when(vehiclesRepository.getByField("vehicleId", vehicle.getVehicleId()))
                .thenReturn(vehicle);

        // Act
        var result = vehiclesService.getVehicleById(vehicle.getVehicleId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(vehicle.getVehicleId(), result.getVehicleId()),
                () -> Assertions.assertEquals(vehicle.getModel(), result.getModel()),
                () -> Assertions.assertEquals(vehicle.getYear(), result.getYear())
        );
    }

    @Test
    void getByModelAndYear_should_callRepository() {
        // Arrange
        var vehicle = createMockVehicle("model", "brand");
        Mockito.when(vehiclesRepository.getVehicleByModelAndYear(vehicle.getModel().getModelId(), vehicle.getYear()))
                .thenReturn(vehicle);

        // Act
        var result = vehiclesService.getVehicleByModelAndYear(vehicle.getModel().getModelId(), vehicle.getYear());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(vehicle.getVehicleId(), result.getVehicleId()),
                () -> Assertions.assertEquals(vehicle.getModel(), result.getModel()),
                () -> Assertions.assertEquals(vehicle.getYear(), result.getYear())
        );
    }

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(vehiclesRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        vehiclesService.getAllVehicles();

        // Assert
        Mockito.verify(vehiclesRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void createVehicle_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var vehicle = createMockVehicle("model", "brand");
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> vehiclesService.createVehicle(vehicle, user));
    }

    @Test
    void createVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var vehicle = createMockVehicle("model", "brand");

        Mockito.doNothing().when(vehiclesRepository).create(vehicle);

        // Act
        vehiclesService.createVehicle(vehicle, admin);

        // Assert
        Mockito.verify(vehiclesRepository, Mockito.times(1))
                .create(vehicle);
    }

    @Test
    void updateVehicle_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var vehicle = createMockVehicle("model", "brand");
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> vehiclesService.updateVehicle(vehicle, user));
    }

    @Test
    void updateVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var vehicle = createMockVehicle("model", "brand");

        Mockito.doNothing().when(vehiclesRepository).update(vehicle);

        // Act
        vehiclesService.updateVehicle(vehicle, admin);

        // Assert
        Mockito.verify(vehiclesRepository, Mockito.times(1))
                .update(vehicle);
    }

    @Test
    void deleteVehicle_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var vehicle = createMockVehicle("model", "brand");
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> vehiclesService.deleteVehicle(vehicle.getVehicleId(), user));
    }

    @Test
    void deleteVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var vehicle = createMockVehicle("model", "brand");

        Mockito.doNothing().when(vehiclesRepository).delete(vehicle.getVehicleId());

        // Act
        vehiclesService.deleteVehicle(vehicle.getVehicleId(), admin);

        // Assert
        Mockito.verify(vehiclesRepository, Mockito.times(1))
                .delete(vehicle.getVehicleId());
    }
}
