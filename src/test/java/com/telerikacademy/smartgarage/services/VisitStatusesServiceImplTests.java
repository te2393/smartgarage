package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.repositories.contracts.VisitStatusesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.telerikacademy.smartgarage.Helpers.createMockVisitStatus;

@ExtendWith(MockitoExtension.class)
public class VisitStatusesServiceImplTests {

    @Mock
    VisitStatusesRepository visitStatusesRepository;

    @InjectMocks
    VisitStatusesServiceImpl visitStatusesService;

    @Test
    void getById_should_callRepository() {
        // Arrange
        var status = createMockVisitStatus();
        Mockito.when(visitStatusesRepository.getByField("statusId", status.getStatusId()))
                .thenReturn(status);

        // Act
        var result = visitStatusesService.getStatusById(status.getStatusId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(status.getStatusId(), result.getStatusId()),
                () -> Assertions.assertEquals(status.getStatusName(), result.getStatusName())
        );
    }
}
