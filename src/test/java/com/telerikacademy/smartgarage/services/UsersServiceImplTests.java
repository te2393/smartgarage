package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicateEmailException;
import com.telerikacademy.smartgarage.exceptions.users.DuplicatePhoneNumberException;
import com.telerikacademy.smartgarage.repositories.contracts.RegisteredVehicleRepository;
import com.telerikacademy.smartgarage.repositories.contracts.UsersRepository;
import com.telerikacademy.smartgarage.repositories.contracts.VisitsRepository;
import com.telerikacademy.smartgarage.utils.EmailHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.smartgarage.Helpers.createMockAdmin;
import static com.telerikacademy.smartgarage.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class UsersServiceImplTests {

    @Mock
    RegisteredVehicleRepository vehicleRepository;

    @Mock
    VisitsRepository visitsRepository;

    @Mock
    EmailHelper emailHelper;

    @Mock
    UsersRepository usersRepository;

    @InjectMocks
    UsersServiceImpl usersService;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(usersRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        usersService.getAllUsers();

        // Assert
        Mockito.verify(usersRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getById_should_callRepository() {
        // Arrange
        var user = createMockUser();
        Mockito.when(usersRepository.getByField("userId", user.getUserId()))
                .thenReturn(user);

        // Act
        var result = usersService.getUserById(user.getUserId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(user.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(user.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(user.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(user.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(user.getPhoneNumber(), result.getPhoneNumber()),
                () -> Assertions.assertEquals(user.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(user.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(user.getUserRole(), result.getUserRole())
        );
    }

    @Test
    void getByEmail_should_callRepository() {
        // Arrange
        var user = createMockUser();
        Mockito.when(usersRepository.getByField("email", user.getEmail()))
                .thenReturn(user);

        // Act
        var result = usersService.getByEmail(user.getEmail());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(user.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(user.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(user.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(user.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(user.getPhoneNumber(), result.getPhoneNumber()),
                () -> Assertions.assertEquals(user.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(user.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(user.getUserRole(), result.getUserRole())
        );
    }

    @Test
    void getByUsername_should_callRepository() {
        // Arrange
        var user = createMockUser();
        Mockito.when(usersRepository.getByField("username", user.getUsername()))
                .thenReturn(user);

        // Act
        var result = usersService.getUserByUsername(user.getUsername());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(user.getUserId(), result.getUserId()),
                () -> Assertions.assertEquals(user.getFirstName(), result.getFirstName()),
                () -> Assertions.assertEquals(user.getLastName(), result.getLastName()),
                () -> Assertions.assertEquals(user.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(user.getPhoneNumber(), result.getPhoneNumber()),
                () -> Assertions.assertEquals(user.getPassword(), result.getPassword()),
                () -> Assertions.assertEquals(user.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(user.getUserRole(), result.getUserRole())
        );
    }

    @Test
    void getAllCustomers_should_callRepository() {
        // Arrange
        Mockito.when(usersRepository.getAllCustomers())
                .thenReturn(new ArrayList<>());

        // Act
        usersService.getAllCustomers();

        // Assert
        Mockito.verify(usersRepository, Mockito.times(1))
                .getAllCustomers();
    }

    @Test
    void createUser_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var userToCreate = createMockUser("User", "mock1@mail.com");
        var user = createMockUser("User", "mock2@mail.com");

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> usersService.createUser(userToCreate, user));
    }

    @Test
    void createUser_should_throw_whenUserEmailExists() {
        // Arrange
        var userToCreate = createMockUser("User", "mock1@mail.com");
        var admin = createMockAdmin();

        Mockito.when(usersRepository.getByField("email", userToCreate.getEmail()))
                .thenReturn(userToCreate);
        Mockito.when(usersRepository.getByField("phoneNumber", userToCreate.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEmailException.class,
                () -> usersService.createUser(userToCreate, admin));
    }

    @Test
    void createUser_should_throw_whenUserPhoneExists() {
        // Arrange
        var userToCreate = createMockUser("User", "mock1@mail.com");
        var admin = createMockAdmin();

        Mockito.when(usersRepository.getByField("email", userToCreate.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(usersRepository.getByField("phoneNumber", userToCreate.getPhoneNumber()))
                .thenReturn(userToCreate);

        // Act, Assert
        Assertions.assertThrows(
                DuplicatePhoneNumberException.class,
                () -> usersService.createUser(userToCreate, admin));
    }

    @Test
    void createUser_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();

        Mockito.doNothing().when(usersRepository).create(user);
        Mockito.doNothing().when(emailHelper).constructLoginCredentialsEmail(user);
        Mockito.when(usersRepository.getByField("email", user.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(usersRepository.getByField("phoneNumber", user.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        usersService.createUser(user, admin);

        // Assert
        Mockito.verify(usersRepository, Mockito.times(1))
                .create(user);
    }

    @Test
    void updateUser_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var userToUpdate = createMockUser("User", "mock1@mail.com");
        var user = createMockUser("User", "mock2@mail.com");

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> usersService.updateUser(userToUpdate, user));
    }

    @Test
    void updateUser_should_throw_whenUserEmailExists() {
        // Arrange
        var userToUpdate = createMockUser("User", "mock1@mail.com");
        var admin = createMockAdmin();

        Mockito.when(usersRepository.getByField("email", userToUpdate.getEmail()))
                .thenReturn(userToUpdate);
        Mockito.when(usersRepository.getByField("phoneNumber", userToUpdate.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEmailException.class,
                () -> usersService.updateUser(userToUpdate, admin));
    }

    @Test
    void updateUser_should_throw_whenUserPhoneExists() {
        // Arrange
        var userToUpdate = createMockUser("User", "mock1@mail.com");
        var admin = createMockAdmin();

        Mockito.when(usersRepository.getByField("email", userToUpdate.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(usersRepository.getByField("phoneNumber", userToUpdate.getPhoneNumber()))
                .thenReturn(userToUpdate);

        // Act, Assert
        Assertions.assertThrows(
                DuplicatePhoneNumberException.class,
                () -> usersService.updateUser(userToUpdate, admin));
    }

    @Test
    void updateUser_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();

        Mockito.doNothing().when(usersRepository).update(user);
        Mockito.when(usersRepository.getByField("email", user.getEmail()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(usersRepository.getByField("phoneNumber", user.getPhoneNumber()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        usersService.updateUser(user, admin);

        // Assert
        Mockito.verify(usersRepository, Mockito.times(1))
                .update(user);
    }

    @Test
    void deleteUser_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var userToDelete = createMockUser("User", "mock1@mail.com");
        var user = createMockUser("User", "mock2@mail.com");

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> usersService.deleteUser(userToDelete.getUserId(), user));
    }

    @Test
    void deleteUser_should_callRepositories_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();

        Mockito.doNothing().when(usersRepository).delete(user.getUserId());
        Mockito.doNothing().when(visitsRepository).makeVisitsWithDefaultVehicle(user.getUserId());
        Mockito.doNothing().when(vehicleRepository).deleteVehiclesByOwner(user.getUserId());

        // Act
        usersService.deleteUser(user.getUserId(), admin);

        // Assert
        Mockito.verify(usersRepository, Mockito.times(1))
                .delete(user.getUserId());
    }

    @Test
    void searchUser_should_throw_whenOperatingUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> usersService.search(Optional.of("keyword"), Optional.of("type"), user));
    }

    @Test
    void searchUser_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();

        Mockito.when(usersRepository.search(Optional.of("keyword"), Optional.of("type")))
                .thenReturn(new ArrayList<>());

        // Act
        usersService.search(Optional.of("keyword"), Optional.of("type"), admin);

        // Assert
        Mockito.verify(usersRepository, Mockito.times(1))
                .search(Optional.of("keyword"), Optional.of("type"));
    }
}
