package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Brands;
import com.telerikacademy.smartgarage.models.Models;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.contracts.ModelsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ModelsServiceImplTests {

    @Mock
    ModelsRepository modelsRepository;

    @InjectMocks
    ModelsServiceImpl modelsService;

    @Test
    void getBy_should_callRepository() {
        // Arrange
        Models model = createMockModel("Mockmodel", "Mockbrand");
        Mockito.when(modelsRepository.getByField("modelId", model.getModelId())).thenReturn(model);

        // Act
        Models result = modelsService.getModelById(999);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(model.getModelId(), result.getModelId()),
                () -> Assertions.assertEquals(model.getModelName(), result.getModelName()),
                () -> Assertions.assertEquals(model.getBrand(), result.getBrand())
        );
    }

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(modelsRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        modelsService.getAllModels();

        // Assert
        Mockito.verify(modelsRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void createModel_should_throw_whenUserIsNotAdmin() {
        // Arrange
        Models model = createMockModel("Mockmodel", "Mockbrand");
        Users user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> modelsService.createModel(model, user));
    }

    @Test
    void createModel_should_callRepository_whenCorrectDetails() {
        // Arrange
        Models model = createMockModel("Mockmodel", "Mockbrand");
        Users admin = createMockAdmin();

        Mockito.doNothing().when(modelsRepository).create(model);

        // Act
        modelsService.createModel(model, admin);

        // Assert
        Mockito.verify(modelsRepository, Mockito.times(1))
                .create(model);
    }

    @Test
    void updateModel_should_throw_whenUserIsNotAdmin() {
        // Arrange
        Models model = createMockModel("Mockmodel", "Mockbrand");
        Users user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> modelsService.updateModel(model, user));
    }

    @Test
    void updateModel_should_callRepository_whenCorrectDetails() {
        // Arrange
        Models model = createMockModel("Mockmodel", "Mockbrand");
        Users admin = createMockAdmin();

        Mockito.doNothing().when(modelsRepository).update(model);

        // Act
        modelsService.updateModel(model, admin);

        // Assert
        Mockito.verify(modelsRepository, Mockito.times(1))
                .update(model);
    }

    @Test
    void deleteModel_should_throw_whenUserIsNotAdmin() {
        // Arrange
        Models model = createMockModel("Mockmodel", "Mockbrand");
        Users user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> modelsService.deleteModel(model.getModelId(), user));
    }

    @Test
    void deleteModel_should_callRepository_whenCorrectDetails() {
        // Arrange
        Models model = createMockModel("Mockmodel", "Mockbrand");
        Users admin = createMockAdmin();

        Mockito.doNothing().when(modelsRepository).delete(model.getModelId());

        // Act
        modelsService.deleteModel(model.getModelId(), admin);

        // Assert
        Mockito.verify(modelsRepository, Mockito.times(1))
                .delete(model.getModelId());
    }

    @Test
    void getModelsByBrandId_should_callRepository() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Mockito.when(modelsRepository.getModelsByBrandId(brand.getBrandId()))
                .thenReturn(new ArrayList<>());

        // Act
        modelsService.getModelsByBrandId(brand.getBrandId());

        // Assert
        Mockito.verify(modelsRepository, Mockito.times(1))
                .getModelsByBrandId(brand.getBrandId());
    }

}
