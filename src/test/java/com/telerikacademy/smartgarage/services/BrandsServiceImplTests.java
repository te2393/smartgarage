package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.models.Brands;
import com.telerikacademy.smartgarage.models.Users;
import com.telerikacademy.smartgarage.repositories.contracts.BrandsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.telerikacademy.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class BrandsServiceImplTests {

    @Mock
    BrandsRepository brandsRepository;

    @InjectMocks
    BrandsServiceImpl brandsService;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(brandsRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        brandsService.getAllBrands();

        // Assert
        Mockito.verify(brandsRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getBy_should_callRepository() {
        // Arrange
        Brands brand = createMockBrand("MockBrand");
        Mockito.when(brandsRepository.getByField("brandId", brand.getBrandId())).thenReturn(brand);

        // Act
        Brands result = brandsService.getBrandById(999);

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(brand.getBrandId(), result.getBrandId()),
                () -> Assertions.assertEquals(brand.getBrandName(), result.getBrandName())
        );
    }

    @Test
    void createBrand_should_throw_whenUserIsNotAdmin() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> brandsService.createBrand(brand, user));
    }

    @Test
    void createBrand_should_throw_whenBrandNameExists() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users admin = createMockAdmin();
        Mockito.when(brandsRepository.getByField("brandName", brand.getBrandName()))
                .thenReturn(brand);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> brandsService.createBrand(brand, admin));
    }

    @Test
    void createBrand_should_callRepository_whenCorrectDetails() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users admin = createMockAdmin();

        Mockito.doNothing().when(brandsRepository).create(brand);
        Mockito.when(brandsRepository.getByField("brandName", brand.getBrandName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        brandsService.createBrand(brand, admin);

        // Assert
        Mockito.verify(brandsRepository, Mockito.times(1))
                .create(brand);
    }

    @Test
    void updateBrand_should_throw_whenUserIsNotAdmin() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> brandsService.updateBrand(brand, user));
    }

    @Test
    void updateBrand_should_throw_whenBrandNameExists() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users admin = createMockAdmin();
        Mockito.when(brandsRepository.getByField("brandName", brand.getBrandName()))
                .thenReturn(brand);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> brandsService.updateBrand(brand, admin));
    }

    @Test
    void updateBrand_should_callRepository_whenCorrectDetails() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users admin = createMockAdmin();

        Mockito.doNothing().when(brandsRepository).update(brand);
        Mockito.when(brandsRepository.getByField("brandName", brand.getBrandName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        brandsService.updateBrand(brand, admin);

        // Assert
        Mockito.verify(brandsRepository, Mockito.times(1))
                .update(brand);
    }

    @Test
    void deleteBrand_should_throw_whenUserIsNotAdmin() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> brandsService.deleteBrand(brand.getBrandId(), user));
    }

    @Test
    void deleteBrand_should_callRepository_whenCorrectDetails() {
        // Arrange
        Brands brand = createMockBrand("Mockbrand");
        Users admin = createMockAdmin();

        Mockito.doNothing().when(brandsRepository).delete(brand.getBrandId());

        // Act
        brandsService.deleteBrand(brand.getBrandId(), admin);

        // Assert
        Mockito.verify(brandsRepository, Mockito.times(1))
                .delete(brand.getBrandId());
    }
}
