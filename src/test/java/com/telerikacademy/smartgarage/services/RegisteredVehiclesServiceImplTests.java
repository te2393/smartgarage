package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.RegisteredVehicleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class RegisteredVehiclesServiceImplTests {

    @Mock
    RegisteredVehicleRepository rvRepository;

    @InjectMocks
    RegisteredVehiclesServiceImpl rvService;

    @Test
    void getById_should_callRepository() {
        // Arrange
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");
        Mockito.when(rvRepository.getByField("registeredVehicleId", vehicle.getRegisteredVehicleId()))
                .thenReturn(vehicle);

        // Act
        var result = rvService.getRegisteredVehicleById(vehicle.getRegisteredVehicleId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(vehicle.getRegisteredVehicleId(), result.getRegisteredVehicleId()),
                () -> Assertions.assertEquals(vehicle.getVehicle(), result.getVehicle()),
                () -> Assertions.assertEquals(vehicle.getLicensePlate(), result.getLicensePlate()),
                () -> Assertions.assertEquals(vehicle.getVin(), result.getVin()),
                () -> Assertions.assertEquals(vehicle.getOwner(), result.getOwner())
        );
    }

    @Test
    void getByLicensePlate_should_callRepository() {
        // Arrange
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");
        Mockito.when(rvRepository.getByField("licensePlate", vehicle.getLicensePlate()))
                .thenReturn(vehicle);

        // Act
        var result = rvService.getRegisteredVehicleByLicensePlate(vehicle.getLicensePlate());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(vehicle.getRegisteredVehicleId(), result.getRegisteredVehicleId()),
                () -> Assertions.assertEquals(vehicle.getVehicle(), result.getVehicle()),
                () -> Assertions.assertEquals(vehicle.getLicensePlate(), result.getLicensePlate()),
                () -> Assertions.assertEquals(vehicle.getVin(), result.getVin()),
                () -> Assertions.assertEquals(vehicle.getOwner(), result.getOwner())
        );
    }

    @Test
    void getByUser_should_callRepository() {
        // Arrange
        var user = createMockUser();
        Mockito.when(rvRepository.getVehiclesByOwner(user.getUserId()))
                .thenReturn(new ArrayList<>());

        // Act
        rvService.getVehiclesByOwner(user.getUserId());

        // Assert
        Mockito.verify(rvRepository, Mockito.times(1))
                .getVehiclesByOwner(user.getUserId());
    }

    @Test
    void getAll_should_callRepository() {
        // Arrange
        var user = createMockUser();
        Mockito.when(rvRepository.getAll())
                .thenReturn(new ArrayList<>());

        // Act
        rvService.getAllVehicles();

        // Assert
        Mockito.verify(rvRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void createRegVehicle_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> rvService.createRegisteredVehicle(vehicle, user));
    }

    @Test
    void createRegVehicle_should_throw_whenLicensePlateExists() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        Mockito.when(rvRepository.getByField("licensePlate", vehicle.getLicensePlate()))
                .thenReturn(vehicle);
        Mockito.when(rvRepository.getByField("vin", vehicle.getVin()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> rvService.createRegisteredVehicle(vehicle, admin));
    }

    @Test
    void createRegVehicle_should_throw_whenVinExists() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        Mockito.when(rvRepository.getByField("licensePlate", vehicle.getLicensePlate()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(rvRepository.getByField("vin", vehicle.getVin()))
                .thenReturn(vehicle);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> rvService.createRegisteredVehicle(vehicle, admin));
    }

    @Test
    void createRegVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        Mockito.doNothing().when(rvRepository).create(vehicle);
        Mockito.when(rvRepository.getByField("licensePlate", vehicle.getLicensePlate()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(rvRepository.getByField("vin", vehicle.getVin()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        rvService.createRegisteredVehicle(vehicle, admin);

        // Assert
        Mockito.verify(rvRepository, Mockito.times(1))
                .create(vehicle);
    }

    @Test
    void updateRegVehicle_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> rvService.updateRegisteredVehicle(vehicle, user));
    }

    @Test
    void updateRegVehicle_should_throw_whenLicensePlateExists() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        Mockito.when(rvRepository.getByField("licensePlate", vehicle.getLicensePlate()))
                .thenReturn(vehicle);
        Mockito.when(rvRepository.getByField("vin", vehicle.getVin()))
                .thenThrow(EntityNotFoundException.class);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> rvService.updateRegisteredVehicle(vehicle, admin));
    }

    @Test
    void updateRegVehicle_should_throw_whenVinExists() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        Mockito.when(rvRepository.getByField("licensePlate", vehicle.getLicensePlate()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(rvRepository.getByField("vin", vehicle.getVin()))
                .thenReturn(vehicle);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> rvService.updateRegisteredVehicle(vehicle, admin));
    }

    @Test
    void updateRegVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        Mockito.doNothing().when(rvRepository).update(vehicle);
        Mockito.when(rvRepository.getByField("licensePlate", vehicle.getLicensePlate()))
                .thenThrow(EntityNotFoundException.class);
        Mockito.when(rvRepository.getByField("vin", vehicle.getVin()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        rvService.updateRegisteredVehicle(vehicle, admin);

        // Assert
        Mockito.verify(rvRepository, Mockito.times(1))
                .update(vehicle);
    }

    @Test
    void deleteRegVehicle_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> rvService.deleteVehicle(vehicle.getRegisteredVehicleId(), user));
    }

    @Test
    void deleteRegVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var user = createMockUser();
        var vehicle = createMockRegVehicle(user, "model", "brand");

        Mockito.doNothing().when(rvRepository).delete(vehicle.getRegisteredVehicleId());

        // Act
        rvService.deleteVehicle(vehicle.getRegisteredVehicleId(), admin);

        // Assert
        Mockito.verify(rvRepository, Mockito.times(1))
                .delete(vehicle.getRegisteredVehicleId());
    }

    @Test
    void filterRegVehicle_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> rvService.filter(
                        Optional.of(2022), Optional.of(999), Optional.of(999), Optional.of("sort"), user));
    }

    @Test
    void filterRegVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();

        Mockito.when(rvRepository.filter(Optional.of(2022), Optional.of(999), Optional.of(999), Optional.of("sort")))
                .thenReturn(new ArrayList<>());

        // Act
        rvService.filter(Optional.of(2022), Optional.of(999), Optional.of(999), Optional.of("sort"), admin);

        // Assert
        Mockito.verify(rvRepository, Mockito.times(1))
                .filter(Optional.of(2022), Optional.of(999), Optional.of(999), Optional.of("sort"));
    }

    @Test
    void searchRegVehicle_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> rvService.search(Optional.of("value"), Optional.of("type"), user));
    }

    @Test
    void searchRegVehicle_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();

        Mockito.when(rvRepository.search(Optional.of("value"), Optional.of("type")))
                .thenReturn(new ArrayList<>());

        // Act
        rvService.search(Optional.of("value"), Optional.of("type"), admin);

        // Assert
        Mockito.verify(rvRepository, Mockito.times(1))
                .search(Optional.of("value"), Optional.of("type"));
    }
}
