package com.telerikacademy.smartgarage.services;

import com.telerikacademy.smartgarage.exceptions.DuplicateEntityException;
import com.telerikacademy.smartgarage.exceptions.EntityNotFoundException;
import com.telerikacademy.smartgarage.exceptions.UnauthorizedOperationException;
import com.telerikacademy.smartgarage.repositories.contracts.ServicesRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.telerikacademy.smartgarage.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ServicesServiceImplTests {

    @Mock
    ServicesRepository servicesRepository;

    @InjectMocks
    ServicesServiceImpl servicesService;

    @Test
    void getAll_should_callRepository() {
        // Arrange
        Mockito.when(servicesRepository.getAllServices())
                .thenReturn(new ArrayList<>());

        // Act
        servicesService.getAllServices();

        // Assert
        Mockito.verify(servicesRepository, Mockito.times(1))
                .getAllServices();
    }

    @Test
    void getById_should_callRepository() {
        // Arrange
        var service = createMockService("name", 100);
        Mockito.when(servicesRepository.getByField("serviceId", service.getServiceId()))
                .thenReturn(service);

        // Act
        var result = servicesService.getServiceById(service.getServiceId());

        // Assert
        Assertions.assertAll(
                () -> Assertions.assertEquals(service.getServiceId(), result.getServiceId()),
                () -> Assertions.assertEquals(service.getServiceName(), result.getServiceName()),
                () -> Assertions.assertEquals(service.getPrice(), result.getPrice()),
                () -> Assertions.assertEquals(service.getVisits(), result.getVisits())
        );
    }

    @Test
    void createService_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();
        var service = createMockService("name", 100);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> servicesService.createService(service, user));
    }

    @Test
    void createService_should_throw_whenServiceNameExists() {
        // Arrange
        var admin = createMockAdmin();
        var service = createMockService("name", 100);

        Mockito.when(servicesRepository.getByField("serviceName", service.getServiceName()))
                .thenReturn(service);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> servicesService.createService(service, admin));
    }

    @Test
    void createService_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var service = createMockService("name", 100);

        Mockito.doNothing().when(servicesRepository).create(service);
        Mockito.when(servicesRepository.getByField("serviceName", service.getServiceName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        servicesService.createService(service, admin);

        // Assert
        Mockito.verify(servicesRepository, Mockito.times(1))
                .create(service);
    }

    @Test
    void updateService_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();
        var service = createMockService("name", 100);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> servicesService.updateService(service, user));
    }

    @Test
    void updateService_should_throw_whenServiceNameExists() {
        // Arrange
        var admin = createMockAdmin();
        var service = createMockService("name", 100);

        Mockito.when(servicesRepository.getByField("serviceName", service.getServiceName()))
                .thenReturn(service);

        // Act, Assert
        Assertions.assertThrows(
                DuplicateEntityException.class,
                () -> servicesService.updateService(service, admin));
    }

    @Test
    void updateService_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var service = createMockService("name", 100);

        Mockito.doNothing().when(servicesRepository).update(service);
        Mockito.when(servicesRepository.getByField("serviceName", service.getServiceName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        servicesService.updateService(service, admin);

        // Assert
        Mockito.verify(servicesRepository, Mockito.times(1))
                .update(service);
    }

    @Test
    void deleteService_should_throw_whenUserIsNotAdmin() {
        // Arrange
        var user = createMockUser();
        var service = createMockService("name", 100);

        // Act, Assert
        Assertions.assertThrows(
                UnauthorizedOperationException.class,
                () -> servicesService.deleteService(service.getServiceId(), user));
    }

    @Test
    void deleteService_should_callRepository_whenCorrectDetails() {
        // Arrange
        var admin = createMockAdmin();
        var service = createMockService("name", 100);

        Mockito.doNothing().when(servicesRepository).deleteService(service.getServiceId());

        // Act
        servicesService.deleteService(service.getServiceId(), admin);

        // Assert
        Mockito.verify(servicesRepository, Mockito.times(1))
                .deleteService(service.getServiceId());
    }

    @Test
    void filterService_should_callRepository_whenCorrectDetails() {
        // Arrange
        Mockito.when(servicesRepository.filter(Optional.of("name"), Optional.of(1.00), Optional.of(100.00),
                        Optional.of("sort")))
                .thenReturn(new ArrayList<>());

        // Act
        servicesService.filter(Optional.of("name"), Optional.of(1.00), Optional.of(100.00), Optional.of("sort"));

        // Assert
        Mockito.verify(servicesRepository, Mockito.times(1))
                .filter(Optional.of("name"), Optional.of(1.00), Optional.of(100.00), Optional.of("sort"));
    }

    @Test
    void searchService_should_callRepository_whenCorrectDetails() {
        // Arrange
        Mockito.when(servicesRepository.search(Optional.of("keyword")))
                .thenReturn(new ArrayList<>());

        // Act
        servicesService.search(Optional.of("keyword"));

        // Assert
        Mockito.verify(servicesRepository, Mockito.times(1))
                .search(Optional.of("keyword"));
    }
}
