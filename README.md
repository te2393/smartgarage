# Smart Garage

A Java project by Yordan Bradushev and Ivan Petkov for the learning purposes of Telerik Academy.

Smart Garage is an application built to be used by the employees of an auto-repair shop to manage their day to day services.

![Login page](https://i.ibb.co/sFd1GFY/login.jpg)
![Add service page](https://i.ibb.co/Fwp6wjC/add-service.jpg)
![View users page](https://i.ibb.co/Jt6KVHc/view-users.jpg)

## Prerequisites

If you want to run the project locally, you will need: IntelliJ and Java SDK

## Getting started

1. Configure application.properties file to use your personal details

2. Run the database_create.sql and database_populate.sql files to configure the database

3. Enjoy!

## Built with

- [Spring](https://spring.io/) - Web framework used
- [Gradle](https://gradle.org/) - Dependency management
- [Thymeleaf](https://www.thymeleaf.org/) - Used to manage the MVC
- [JUnit](https://junit.org/junit5/) and [Mockito](https://site.mockito.org/) - Testing frameworks used
- [JavaScript/jQuery](https://jquery.com/) - Used to manipulate HTML
- [Bootstrap](https://getbootstrap.com/) - Used to manipulate CSS
- [Currency Converter API](https://www.currencyconverterapi.com/) - Used for the currency conversion
- [iText](https://itextpdf.com/en) - Used to generate PDFs


## Swagger Documentation
When you run the project, you can read api documentation on this [link](http://localhost:8080/swagger-ui/)

## Database relations

![db-schema](https://i.ibb.co/xjd1WYT/db-schema.jpg)

## Authors and acknowledgment

- **Yordan Bradushev**
- **Ivan Petkov**

_One of the many dream teams in Telerik Academy who will be forever greatful to their trainers, colleagues and mentors_


